// context.h - declare game context

// Copyright (C) 2021-2023 L. Sanz <luis.sanz@gmail.com>

// This file is part of Princess and Undead.

// Princess and Undead is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Princess and Undead is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Princess and Undead.  If not, see <https://www.gnu.org/licenses/>.


#ifndef SRC_CONTEXT_H_
#define SRC_CONTEXT_H_

#include <vector>
#include <filesystem>
#include <bitset>
#include <list>

#include "physics.h"
#include "render.h"
#include "mobs.h"
#include "princess.h"

// Defines a place where a mob will appear
struct Mob_place {
        Mob_type type;
        points32b box;
};

// Defines a room
struct Room {
        points32b box{0, 0, 0, 0};
        size_t first_place{0};
        size_t last_place{0};
};

// Input mask
struct Input {
        bool up = false;
        bool down = false;
        bool left = false;
        bool right = false;
        bool jump = false;
        bool attack = false;
        bool terminate = false;
};

// Indices of all sound effects
enum Sound {
        bounce, hurt, jump, land, step, swing, mob_die, coin, death, ping
};

// Extra data required for the mobs
struct Context {
        Input input;                    // Defines which inputs are toggled.
        Princess princess;              // Data for the player character
        std::vector<Room> rooms;        // List of all rooms in the scene
        Room room;                      // Current room in the scene
        pixels16v offset{0, 0};         // Top left corner of visible area
        std::vector<Mob_place> places;  // Places where mobs spawn on room change
        std::list<std::unique_ptr<Mob>> mobs; // Mobs actually loaded
        Hitmask hitmask;                // hitmask table
        Nametable nametable;            // background layer
        bool update_nametable{true};    // if true, the nametable has been changed
        std::vector<Sprite> sprites;    // sprite layer
        int frame = 0;                  // Current frame
        int freeze = 0;                 // If greater than zero, the game pauses
        std::bitset<32> sounds;         // Mask of sounds to be played.
};

Context load_context(const std::filesystem::path& path);

#endif // SRC_CONTEXT_H_
