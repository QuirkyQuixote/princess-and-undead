// physics.cc - collision between bodies and hitmask

// Copyright (C) 2021-2023 L. Sanz <luis.sanz@gmail.com>

// This file is part of Princess and Undead.

// Princess and Undead is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Princess and Undead is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Princess and Undead.  If not, see <https://www.gnu.org/licenses/>.


#include "physics.h"

#include <algorithm>

// Test first movement in the x axis, the in the y axis.  If collision happens,
// the position is set to the collision point and velocity that axis set to
// zero; if not, position is incremented by velocity.

// For each axis of movement, there are three possible cases:

// - having negative speed, so the body may collide with a right/bottom side;
// - having positive speed, so the body may collide with a left/top side;
// - having no speed, so the body may collide with any side.

// I could have implemented these three functions for each axis; instead, the
// functions to test for collision are templated on the indices of the primary
// and secondary axes, that are accessed through Vec<S, T>::operator[].

template<size_t X, class Box>
Box sweep_pos(const Box& b, geom::geom_value_t<Box> amount)
{
        if constexpr (X == 0) return Box(b.r, b.r + amount, b.d, b.u);
        else return Box(b.l, b.r, b.u, b.u + amount);
}

template<size_t X, class Box>
Box sweep_neg(const Box& b, geom::geom_value_t<Box> amount)
{
        if constexpr (X == 0) return Box(b.l + amount, b.l, b.d, b.u);
        else return Box(b.l, b.r, b.d + amount, b.d);
}

template<size_t X> bool test_pos(Body& body, Hitmask& hitmask)
{
        auto box = geom::out_cast<cells16>(sweep_pos<X>(body.box, body.vel[X]));
        auto slice = geom::slice(hitmask, box);
        auto it = std::ranges::find(slice, true);
        if (it != slice.end()) {
                auto s = body.box[X].max - body.box[X].min;
                body.box[X].max = box[X].min + geom::path(slice.begin(), it)[X];
                body.box[X].min = body.box[X].max - s;
                if (body.bouncy) body.vel[X] /= -2;
                else body.vel[X] = 0_pt;
                return true;
        }
        body.box[X] += body.vel[X];
        return false;
}

template<size_t X> bool test_neg(Body& body, Hitmask& hitmask)
{
        auto box = geom::out_cast<cells16>(sweep_neg<X>(body.box, body.vel[X]));
        auto slice = geom::slice(hitmask, box);
        auto it = std::ranges::find(slice, true);
        if (it != slice.end()) {
                auto s = body.box[X].max - body.box[X].min;
                body.box[X].min = box[X].min + geom::path(slice.begin(), it)[X] + 1_cl;
                body.box[X].max = body.box[X].min + s;
                if (body.bouncy) body.vel[X] /= -2;
                else body.vel[X] = 0_pt;
                return true;
        }
        body.box[X] += body.vel[X];
        return false;
}

template<size_t X> bool kiss_pos(Body& body, Hitmask& hitmask)
{
        if (points32(cells16(body.box[X].max)) != body.box[X].max) return false;
        auto box = geom::out_cast<cells16>(sweep_pos<X>(body.box, 1_pt));
        auto slice = geom::slice(hitmask, box);
        return std::ranges::find(slice, true) != slice.end();
}

template<size_t X> bool kiss_neg(Body& body, Hitmask& hitmask)
{
        if (points32(cells16(body.box[X].min)) != body.box[X].min) return false;
        auto box = geom::out_cast<cells16>(sweep_neg<X>(body.box, -1_pt));
        auto slice = geom::slice(hitmask, box);
        return std::ranges::find(slice, true) != slice.end();
}

template<size_t X> void move_axis(Body& body, Hitmask& hitmask)
{
        if (body.vel[X] > 0_pt) {
                body.contact[X].min = test_pos<X>(body, hitmask);
                body.contact[X].max = false;
        } else if (body.vel[X] < 0_pt) {
                body.contact[X].min = false;
                body.contact[X].max = test_neg<X>(body, hitmask);
        } else {
                body.contact[X].min = kiss_pos<X>(body, hitmask);
                body.contact[X].max = kiss_neg<X>(body, hitmask);
        }
}

void move_body(Body& body, Hitmask& hitmask)
{
        move_axis<0>(body, hitmask);
        move_axis<1>(body, hitmask);
}
