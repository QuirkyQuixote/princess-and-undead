// mobs.cc - game logic for movable objects

// Copyright (C) 2021-2023 L. Sanz <luis.sanz@gmail.com>

// This file is part of Princess and Undead.

// Princess and Undead is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Princess and Undead is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Princess and Undead.  If not, see <https://www.gnu.org/licenses/>.


#include "mobs.h"

#include <algorithm>

#include "geom/table.h"

#include "render.h"
#include "physics.h"
#include "context.h"

void explode(points32v p, Context& context);
void splat(points32v p, Context& context);
void throw_bone(points32v p, Context& context, points32 vel_x);
void spawn_coins(points32v p, Context& context, int n);

// Rottens behave like lemmings: they walk aimlessly until they hit a
// wall, then turn and walk the opposite way.  They are affected by
// gravity and can fall.

struct Rotten : public Mob {
        enum State {
                walk_0, walk_1, walk_2, walk_3,
                walk_4, walk_5, walk_6, walk_7,
                walk_8, walk_9, walk_10, walk_11,
                stun_0, stun_1, stun_2, stun_3, stun_4,
                stun_5, stun_6, stun_7, stun_8, stun_9,
                stun_10, stun_11, stun_12, stun_13, stun_14,
                stun_15, stun_16, stun_17, stun_18, stun_19,
                stun_20, stun_21, stun_22, stun_23, stun_24,
                stun_25, stun_26, stun_27, stun_28, stun_29,
                stun_30, stun_31, stun_32, stun_33, stun_34,
                stun_35, stun_36, stun_37, stun_38, stun_39,
                stun_40, stun_41, stun_42, stun_43, stun_44,
                stun_45, stun_46, stun_47, stun_48, stun_49,
                stun_50, stun_51, stun_52, stun_53, stun_54,
                stun_55, stun_56, stun_57, stun_58, stun_59,
                stun_60, stun_61, stun_62, stun_63, stun_64,
                stun_65, stun_66, stun_67, stun_68, stun_69,
                stun_70, stun_71, stun_72, stun_73, stun_74,
                stun_75, stun_76, stun_77, stun_78, stun_79,
                die,
        };

        enum Flags {
                flip = 1
        };

        uint8_t state{0};  // internal state
        uint8_t flags{0};  // additional state
        Body body;      // physical body

        Rotten(points32b box)
        {
                body.box = box;
        }

        void update(Context& context)
        {
                update_state(context);
                update_body(context);
                check_princess(context);
                check_attack(context);
                update_sprites(context);
        }

        bool dead() const { return state == die; }

        void update_state(Context& context)
        {
                switch (state) {
                 case walk_0 ... walk_11:
                        if (++state > walk_11) state = walk_0;
                        body.vel.x = flags ? -8_pt : 8_pt;
                        break;
                 case stun_0 ... stun_79:
                        if (++state > stun_79) state = walk_0;
                        body.vel.x = 0_pt;
                        break;
                }
        }

        void update_body(Context& context)
        {
                move_body(body, context.hitmask);
                if (!body.contact.d) {
                        body.vel.x = 0_pt;
                        body.vel.y += 8_pt;
                } else if (body.contact.l) {
                        flags = flip;
                } else if (body.contact.r) {
                        flags = 0;
                }
        }

        void check_princess(Context& context)
        {
                if (geom::intersects(body.box, context.princess.body.box)) {
                        if (bounce_princess(context, body.box)) {
                                // fall to the floor
                                state = stun_0;
                        } else if (state < stun_0 &&
                                        harm_princess(context, body.box)) {
                                // do nothing?
                        }
                }
        }

        void check_attack(Context& context)
        {
                if (geom::intersects(body.box, context.princess.attack_box)) {
                        context.sounds[Sound::mob_die] = true;
                        state = die;
                        auto p = geom::center(body.box) + points32v(0, -4);
                        splat(p, context);
                        spawn_coins(p, context, 2);
                        return;
                }
        }

        void update_sprites(Context& context)
        {
                static const Sprite sheet[] = {
                        {{-4, -10}, 0x20}, {{-4, -2}, 0x21}, // walk #1
                        {{-4, -9}, 0x20}, {{-4, -2}, 0x22}, // walk #2
                        {{-4, -10}, 0x20}, {{-4, -2}, 0x23}, // walk #3
                        {{-4, -10}, 0x20}, {{-4, -2}, 0x24}, // walk #4
                        {{-2, -8}, 0x20}, {{-4, -2}, 0x25}, // stunned
                        {{0, -2}, 0x27}, {{-4, -2}, 0x26}, // stunned
                };

                static const int16_t frames[] = {
                        0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, // walk
                        4, 4, 4, 5, 5, 5, 5, 5, 5, 5,
                        5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
                        5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
                        5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
                        5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
                        5, 5, 5, 5, 5, 5, 5, 4, 4, 4,
                        5, 5, 5, 5, 5, 5, 5, 4, 4, 4,
                        5, 5, 5, 5, 5, 5, 5, 4, 4, 4,
                };

                const Sprite* first = sheet + frames[state] * 2;
                uint16_t sprite_flags = flags ? Sprite_flags::fliph : 0;
                Sprite meta{geom::center(body.box), sprite_flags};
                push_sprites(context.sprites, first, first + 2, meta);
        }
};

// Bones track the princess and move in bursts of three steps attempting to get
// close to her while avoiding pits.  If near enough, they throw one of their
// bones at her.

struct Bones : public Mob {
        enum State {
                idle_0, idle_1, idle_2, idle_3,
                idle_4, idle_5, idle_6, idle_7,
                walk_0, walk_1, walk_2, walk_3,
                walk_4, walk_5, walk_6, walk_7,
                walk_8, walk_9, walk_10, walk_11,
                walk_12, walk_13, walk_14, walk_15,
                walk_16, walk_17, walk_18, walk_19,
                walk_20, walk_21, walk_22, walk_23,
                throw_0, throw_1, throw_2, throw_3,
                throw_4, throw_5, throw_6, throw_7,
                throw_8, throw_9, throw_10, throw_11,
                throw_12, throw_13, throw_14, throw_15,
                die,
        };

        enum Flags {
                flip = 1
        };

        uint8_t state{0};
        uint8_t flags{0};
        Body body;

        Bones(points32b box)
        {
                body.box = box;
        }

        void update(Context& context)
        {
                update_state(context);
                update_body(context);
                check_princess(context);
                check_attack(context);
                update_sprites(context);
        }

        bool dead() const { return state == die; };

        // Bones track the princess; they move towards the princess in bursts
        // of three steps, then stop for a little.  They avoid pits, and stop
        // walking if they are about to step into one.  If they are near the
        // princess, they throw a bone to her.

        void update_state(Context& context)
        {
                switch (state) {
                 case idle_7:
                        {
                        points32 d = context.princess.body.box.l - body.box.l;
                        if (d < -256_pt) {
                                state = walk_0;
                                flags = flip;
                        } else if (d < 0_pt) {
                                state = throw_0;
                                flags = flip;
                        } else if (d < 256_pt) {
                                state = throw_0;
                                flags = 0;
                        } else {
                                state = walk_0;
                                flags = 0;
                        }
                        break;
                        }
                 case walk_23:
                 case throw_15:
                        state = idle_0;
                        break;
                 case throw_2:
                        throw_bone(geom::center(body.box), context, flags ? -8_pt : 8_pt);
                        ++state;
                        break;
                 default:
                        ++state;
                        break;
                }
        }

        // If falling, horizontal speed is zero, and vertical increases by
        // gravity; if standing, horizontal speed depends on the current state
        // (idle, walking left, or walking right); if walking, an additional
        // check is made to avoid falling in pits.

        void update_body(Context& context)
        {
                move_body(body, context.hitmask);
                if (!body.contact.d) {
                        body.vel.x = 0_pt;
                        body.vel.y += 8_pt;
                } else if (state >= walk_0 && state <= walk_23) {
                        if (flags && context.hitmask[bottom_left(body.box)]) {
                                body.vel.x = -8_pt;
                        } else if (!flags && context.hitmask[bottom_right(body.box)]) {
                                body.vel.x = 8_pt;
                        } else {
                                body.vel.x = 0_pt;
                                state = idle_0;
                        }
                } else {
                        body.vel.x = 0_pt;
                }
        }

        void check_princess(Context& context)
        {
                if (geom::intersects(body.box, context.princess.body.box)) {
                        if (bounce_princess(context, body.box)) {
                                // fall to the floor
                        } else if (harm_princess(context, body.box)) {
                                // do nothing?
                        }
                }
        }

        void check_attack(Context& context)
        {
                if (geom::intersects(body.box, context.princess.attack_box)) {
                        context.sounds[Sound::mob_die] = true;
                        state = die;
                        auto p = geom::center(body.box) + points32v(0, -4);
                        explode(p, context);
                        spawn_coins(p, context, 4);
                        return;
                }
        }

        void update_sprites(Context& context)
        {
                static const Sprite sheet[] = {
                        {{-4, -10}, 0x40}, {{-4, -2}, 0x41}, // idle
                        {{-4, -10}, 0x40}, {{-4, -2}, 0x42}, // walk #1
                        {{-4, -9}, 0x40}, {{-4, -2}, 0x43}, // walk #2
                        {{-4, -10}, 0x40}, {{-4, -2}, 0x44}, // walk #3
                        {{-4, -10}, 0x40}, {{-4, -2}, 0x45}, // walk #4
                        {{-5, -9}, 0x40}, {{-4, -2}, 0x46}, // attack #1
                        {{-2, -10}, 0x40}, {{-4, -2}, 0x47}, // attack #2
                };
                static const int16_t frames[] = {
                        0, 0, 0, 0, 0, 0, 0, 0, // idle
                        1, 1, 2, 2, 3, 3, 4, 4, 1, 1, 2, 2, 3, 3, 4, 4, 1, 1, 2, 2, 3, 3, 4, 4, // walk
                        5, 5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, // throw
                };
                const Sprite* first = sheet + frames[state] * 2;
                uint16_t sprite_flags = flags ? Sprite_flags::fliph : 0;
                Sprite meta{geom::center(body.box), sprite_flags};
                push_sprites(context.sprites, first, first + 2, meta);
        }
};

// A single bone is thrown by bones

struct Bone : public Mob {
        uint8_t state{0};
        uint8_t flags{0};
        Body body;

        Bone(points32v p, points32 vel_x)
        {
                body.box = geom::make_box(p, p);
                body.vel = geom::make_vec(vel_x, -32_pt);
        }

        void update(Context& context)
        {
                state = (state + 1) % 6;
                update_body(context);
                check_attack(context);
                check_princess(context);
                check_room(context);
                update_sprites(context);
        }

        bool dead() const { return state == 0xff; }

        // The bone flies in a parabolic arc, without colliding with the scene
        void update_body(Context& context)
        {
                body.vel.y += 4_pt;
                body.box += body.vel;
        }

        // Touching the attack box bounces the bone
        void check_attack(Context& context)
        {
                if (geom::intersects(body.box, context.princess.attack_box)) {
                        context.sounds[Sound::ping] = true;
                        body.vel.x *= -1;
                        body.vel.y = -32_pt;
                        flags = 1;
                }
        }

        // Touching a bone harms the princess
        void check_princess(Context& context)
        {
                if (geom::intersects(body.box, context.princess.body.box) &&
                                flags == 0 &&
                                harm_princess(context, body.box)) {
                        state = 0xff;
                        return;
                }
        }

        // Check for the bone exiting the room.  Don't use intersect() because
        // it's allowed to leave from the top until it comes down again.
        void check_room(Context& context)
        {
                if (body.box.l > context.room.box.r ||
                                body.box.r < context.room.box.l ||
                                body.box.d > context.room.box.u) {
                        state = 0xff;
                        return;
                }
        }

        // Render the flying, rotating bone
        void update_sprites(Context& context)
        {
                static const Sprite sheet[] = {
                        {{-4, -4}, 0x48},
                        {{-4, -4}, 0x4A},
                        {{-4, -4}, 0x84A},
                        {{-4, -4}, 0x848},
                        {{-4, -4}, 0x49},
                        {{-4, -4}, 0x449},
                };
                const Sprite* first = sheet + state;
                Sprite meta{geom::center(body.box), 0};
                push_sprites(context.sprites, first, first + 1, meta);
        }
};

// Peepers fly
struct Peeper : public Mob {
        enum State {
                hang, glide,
                fly_0, fly_1, fly_2, fly_3,
                fly_4, fly_5, fly_6, fly_7,
                turn_0, turn_1, turn_2, turn_3,
                turn_4, turn_5, turn_6, turn_7,
                die,
        };

        enum Flags {
                flip = 1
        };

        uint8_t state{0};
        uint8_t flags{0};
        Body body;

        Peeper(points32b box)
        {
                body.box = box;
        }

        void update(Context& context)
        {
                update_state(context);
                update_body(context);
                check_princess(context);
                check_attack(context);
                update_sprites(context);
        }

        bool dead() const { return state == die; }

        // Peepers hang from the ceiling until the princess gets near, then
        // they start gliding towards her.
        //
        // If their horizontal or vertical position goes lower than the
        // princess they bat their wings to gain altitude;  if they go pass the
        // princess for a large margin they turn back to try again in the
        // opposite direction.

        void update_state(Context& context)
        {
                auto d = geom::center(body.box) - geom::center(context.princess.body.box);
                body.vel.y = std::min<points32>(body.vel.y + 1_pt, 16_pt);
                switch (state) {
                 case hang:
                        body.vel.y = 0_pt;
                        if (d.x < 0_pt) {
                                if (d.x > -1024_pt) {
                                        state = glide;
                                        flags = 0;
                                }
                        } else {
                                if (d.x < 1024_pt) {
                                        state = glide;
                                        flags = flip;
                                }
                        }
                        break;
                 case glide:
                        if (flags) {
                                body.vel.x = -12_pt;
                                if (d.x < -512_pt) {
                                        state = turn_0;
                                        flags = 0;
                                } else if (d.y > 0_pt) {
                                        state = fly_0;
                                }
                        } else {
                                body.vel.x = 12_pt;
                                if (d.x > 512_pt) {
                                        state = turn_0;
                                        flags = flip;
                                } else if (d.y > 0_pt) {
                                        state = fly_0;
                                }
                        }
                        break;
                 case fly_2 ... fly_3:
                        body.vel.y = -8_pt;
                        ++state;
                        break;
                 case fly_7:
                        state = glide;
                        break;
                 case turn_0 ... turn_7:
                        if (flags) {
                                body.vel.x -= 2_pt;
                                if (body.vel.x == -12_pt) state = glide;
                                else if (++state == turn_7) state = turn_0;
                        } else {
                                body.vel.x += 2_pt;
                                if (body.vel.x == 12_pt) state = glide;
                                else if (++state == turn_7) state = turn_0;
                        }
                        break;
                 default:
                        ++state;
                        break;
                }
        }

        // Update body position, but don't test for scene collision
        void update_body(Context& context)
        {
                body.box += body.vel;
        }

        // Touching the princess hurts her.
        void check_princess(Context& context)
        {
                if (geom::intersects(body.box, context.princess.body.box)) {
                        if (harm_princess(context, body.box)) {
                                // do nothing?
                        }
                }
        }

        // Being hurt by the princess's attack hurts the peeper.
        void check_attack(Context& context)
        {
                if (geom::intersects(body.box, context.princess.attack_box)) {
                        context.sounds[Sound::mob_die] = true;
                        state = die;
                        auto p = geom::center(body.box);
                        explode(p, context);
                        spawn_coins(p, context, 3);
                        return;
                }
        }

        // Render the peeper
        void update_sprites(Context& context)
        {
                static const Sprite sheet[] = {
                        {{-4, -4}, 0x60}, // hang
                        {{-4, -4}, 0x63}, // glide
                        {{-4, -4}, 0x62}, // fly
                        {{-4, -4}, 0x61}, // fly
                        {{-4, -4}, 0x62}, // fly
                        {{-4, -4}, 0x63}, // fly
                        {{-4, -4}, 0x64}, // turn
                };
                static const int16_t frames[] = {
                        0, // hang
                        1, // glide
                        2, 2, 3, 3, 4, 4, 5, 5, // fly
                        2, 2, 3, 3, 4, 4, 5, 5, // turn
                };
                const Sprite* first = sheet + frames[state];
                uint16_t sprite_flags = flags ? Sprite_flags::fliph : 0;
                Sprite meta{geom::center(body.box), sprite_flags};
                push_sprites(context.sprites, first, first + 1, meta);
        }
};

// Crawlers move always in the same direction, following a surface, unbothered
// by gravity.
struct Crawler : public Mob {
        enum State {
                walk_0, walk_1, walk_2, walk_3, die
        };

        enum Flags {
                top_ccw,
                left_ccw,
                bottom_ccw,
                right_ccw,
        };

        uint8_t state{0};
        uint8_t flags{0};
        Body body;

        Crawler(points32b box)
        {
                body.box = box;
        }

        void update(Context& context)
        {
                update_state(context);
                update_body(context);
                check_princess(context);
                check_attack(context);
                update_sprites(context);
        }

        bool dead() const { return state == die; }

        // Crawlers can walk on any surface, but always do it counter
        // clockwise; when they hit a wall head on they turn 90 degrees ccw,
        // and when they "fall" from the surface they are walking on, they turn
        // 90 degrees cw.
        void update_state(Context& context)
        {
                switch (state) {
                 case walk_0 ... walk_3:
                        if (++state > walk_3) state = walk_0;
                        break;
                }
        }

        // The crawler speed depends on the surface it crawls and the direction
        // (clockwise or counter-clockwise)
        void update_body(Context& context)
        {
                switch (flags) {
                 case top_ccw: body.vel = points32v(8, 8); break;
                 case left_ccw: body.vel = points32v(8, -8); break;
                 case bottom_ccw: body.vel = points32v(-8, -8); break;
                 case right_ccw: body.vel = points32v(-8, 8); break;
                }
                move_body(body, context.hitmask);
                switch (flags) {
                 case top_ccw:
                        if (body.contact.l) flags = left_ccw;
                        else if (!body.contact.d) flags = right_ccw;
                        break;
                 case left_ccw:
                        if (body.contact.u) flags = bottom_ccw;
                        else if (!body.contact.l) flags = top_ccw;
                        break;
                 case bottom_ccw:
                        if (body.contact.r) flags = right_ccw;
                        else if (!body.contact.u) flags = left_ccw;
                        break;
                 case right_ccw:
                        if (body.contact.d) flags = top_ccw;
                        else if (!body.contact.r) flags = bottom_ccw;
                        break;
                }
        }

        // If the body touches the princess's, she'll be hurt.
        void check_princess(Context& context)
        {
                if (geom::intersects(body.box, context.princess.body.box))
                        harm_princess(context, body.box);
        }

        // The princess's attack may hurt the crawler
        void check_attack(Context& context)
        {
                if (geom::intersects(body.box, context.princess.attack_box)) {
                        context.sounds[Sound::mob_die] = true;
                        state = die;
                        auto p = geom::center(body.box) + points32v(0, -4);
                        explode(p, context);
                        spawn_coins(p, context, 2);
                        return;
                }
        }

        // Render the crawler
        void update_sprites(Context& context)
        {
                static const Sprite sheet[] = {
                        {{-4, -4}, 0xE0}, // top_ccw
                        {{-4, -4}, 0xE1},
                        {{-4, -4}, 0xE2}, // left_ccw
                        {{-4, -4}, 0xE3},
                        {{-4, -5}, 0xCE0}, // bottom_ccw
                        {{-4, -5}, 0xCE1},
                        {{-5, -4}, 0xCE2}, // right_ccw
                        {{-5, -4}, 0xCE3},
                };
                static const int16_t frames[] = {
                        0, 0, 1, 1, // top_ccw
                        2, 2, 3, 3, // left_ccw
                        4, 4, 5, 5, // bottom_ccw
                        6, 6, 7, 7, // right_ccw
                };
                const Sprite* first = sheet + frames[state + flags * 4];
                Sprite meta{geom::center(body.box), 0};
                push_sprites(context.sprites, first, first + 1, meta);
        }
};

// Cracks are breakable blocks
struct Crack : public Mob {
        Mob_place& place;
        uint8_t state{0};
        Body body;

        Crack(Mob_place& p)
                : place{p}
        {
                body.box = p.box;
        }

        void update(Context& context)
        {
                if (geom::intersects(body.box, context.princess.attack_box)) {
                        auto box = cells16b(body.box);
                        std::ranges::fill(geom::slice(context.nametable, box), 255);
                        std::ranges::fill(geom::slice(context.hitmask, box), false);
                        context.update_nametable = true;
                        context.sounds[Sound::mob_die] = true;
                        place.type = Mob_type::none;
                        state = 1;
                }
        }

        bool dead() const { return state; }
};

struct Mirror : public Mob {
        Body body;

        Mirror(points32b box)
        {
                body.box = box;
        }

        void update(Context& context)
        {
                static const Sprite sheet[] = {
                        {{0, 0}, 0x80}, {{8, 0}, 0x81}, {{16, 0}, 0x481}, {{24, 0}, 0x480},
                        {{0, 8}, 0x882}, {{8, 8}, 0x83}, {{16, 8}, 0x84}, {{24, 8}, 0xC82},
                        {{0, 16}, 0x82}, {{8, 16}, 0x84}, {{16, 16}, 0x83}, {{24, 16}, 0x482},
                        {{0, 24}, 0x882}, {{8, 24}, 0x83}, {{16, 24}, 0x84}, {{24, 24}, 0xC82},
                        {{0, 32}, 0x82}, {{8, 32}, 0x84}, {{16, 32}, 0x83}, {{24, 32}, 0x482},
                        {{0, 40}, 0x880}, {{8, 40}, 0x881}, {{16, 40}, 0xC81}, {{24, 40}, 0xC80},
                };
                Sprite meta{geom::top_left(body.box), 0};
                push_sprites(context.sprites, sheet, sheet + 24, meta);
        }

        bool dead() const { return false; }
};

// Thorns hurt the princess

struct Thorns : public Mob {
        Body body;

        Thorns(points32b box)
        {
                body.box = box;
        }

        void update(Context& context)
        {
                if (geom::intersects(body.box, context.princess.body.box))
                        harm_princess(context, body.box);
        }

        bool dead() const { return false; }
};

struct Explosion : public Mob {
        uint8_t state{0};
        Body body;

        Explosion(points32v p)
        {
                body.box = geom::make_box(p, p);
        }

        void update(Context& context)
        {
                update_sprites(context);
                update_state(context);
        }

        bool dead() const { return state >= 17; }

        void update_state(Context& context)
        {
                ++state;
        }

        void update_sprites(Context& context)
        {
                static const Sprite sheet[] = {
                        {{-8, -8}, 0xA0}, {{0, -8}, 0x4A0}, {{-8, 0}, 0x8A0}, {{0, 0}, 0xCA0},
                        {{-8, -8}, 0xA1}, {{0, -8}, 0x4A1}, {{-8, 0}, 0x8A1}, {{0, 0}, 0xCA1},
                        {{-8, -8}, 0xA8}, {{0, -8}, 0xA9}, {{-8, 0}, 0xAA}, {{0, 0}, 0xAB},
                        {{-8, -8}, 0xAC}, {{0, -8}, 0xAD}, {{-8, 0}, 0xAE}, {{0, 0}, 0xAF},
                        {{-8, -8}, 0xB0}, {{0, -8}, 0xB1}, {{-8, 0}, 0xB2}, {{0, 0}, 0xB3},
                        {{-8, -8}, 0xB4}, {{0, -8}, 0xB5}, {{-8, 0}, 0xB6}, {{0, 0}, 0xB7},
                        {{-8, -8}, 0xB8}, {{0, -8}, 0xB9}, {{-8, 0}, 0xBA}, {{0, 0}, 0xBB},
                };
                static const int16_t frames[] = {
                        0, 1, 2, 3, 3, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6
                };
                const Sprite* first = sheet + frames[state] * 4;
                Sprite meta{geom::top_left(body.box), 0};
                push_sprites(context.sprites, first, first + 4, meta);
        }
};

struct Splatter : public Mob {
        uint8_t state{0};
        Body body;

        Splatter(points32v p)
        {
                body.box = geom::make_box(p, p);
        }

        void update(Context& context)
        {
                update_sprites(context);
                update_state(context);
        }

        bool dead() const { return state >= 8; }

        void update_state(Context& context)
        {
                ++state;
        }

        void update_sprites(Context& context)
        {
                static const Sprite sheet[] = {
                        {{-8, -8}, 0xA0}, {{0, -8}, 0x4A0}, {{-8, 0}, 0x8A0}, {{0, 0}, 0xCA0},
                        {{-8, -8}, 0xA1}, {{0, -8}, 0x4A1}, {{-8, 0}, 0x8A1}, {{0, 0}, 0xCA1},
                        {{-8, -8}, 0xA2}, {{0, -8}, 0xA3}, {{-8, 0}, 0xC2}, {{0, 0}, 0xC3},
                        {{-8, -8}, 0xA4}, {{0, -8}, 0xA5}, {{-8, 0}, 0xC4}, {{0, 0}, 0xC5},
                        {{-8, -8}, 0xA6}, {{0, -8}, 0xA7}, {{-8, 0}, 0xC6}, {{0, 0}, 0xC7},
                };
                static const int16_t frames[] = {
                        0, 1, 2, 2, 3, 3, 4, 4
                };
                const Sprite* first = sheet + frames[state] * 4;
                Sprite meta{geom::top_left(body.box), 0};
                push_sprites(context.sprites, first, first + 4, meta);
        }
};

struct Coin : public Mob {
        uint8_t state{0};
        Body body;

        Coin(points32v p, points32 vel_x)
        {
                body.box = points32b(-8, 8, -8, 8) + p;
                body.vel = geom::make_vec(vel_x, -64_pt);
                body.bouncy = true;
                state = rand() % 8;
        }

        void update(Context& context)
        {
                check_princess(context);
                update_body(context);
                update_sprites(context);
                update_state(context);
        }

        bool dead() const { return state == 0xff; }

        // If the body touches the princess's, the coin is collected
        void check_princess(Context& context)
        {
                if (geom::intersects(body.box, context.princess.body.box)) {
                        context.sounds[Sound::coin] = true;
                        state = 0xff;
                        return;
                }
        }

        // Apply gravity and collision
        void update_body(Context& context)
        {
                body.vel.y += 8_pt;
                move_body(body, context.hitmask);
                if (body.contact.d) {
                        if (body.vel.x > 0_pt) --body.vel.x;
                        else if (body.vel.x < 0_pt) ++body.vel.x;
                }
        }

        // Animate a rotating coin
        void update_sprites(Context& context)
        {
                static const Sprite sheet[] = {
                        {{-3, -6}, 0x85},
                        {{-3, -6}, 0x85},
                        {{-3, -6}, 0x86},
                        {{-3, -6}, 0x86},
                        {{-3, -6}, 0x87},
                        {{-3, -6}, 0x87},
                        {{-3, -6}, 0x88},
                        {{-3, -6}, 0x88},
                };
                const Sprite* first = sheet + state;
                Sprite meta{geom::top_left(body.box), 0};
                push_sprites(context.sprites, first, first + 1, meta);
        }

        void update_state(Context& context)
        {
                if (state != 0xff) state = (state + 1) % 8;
        }
};

struct Dust : public Mob {
        uint8_t state{0};
        Body body;

        Dust(points32v p)
        {
                body.box = geom::make_box(p, p);
        }

        void update(Context& context)
        {
                update_sprites(context);
                update_state(context);
        }

        bool dead() const { return state >= 8; }

        // If the frame of the animation is 8, kill the mob
        void update_state(Context& context)
        {
                ++state;
        }

        // Animate a small dust cloud
        void update_sprites(Context& context)
        {
                static const Sprite sheet[] = {
                        {{-4, -4}, 0x89},
                        {{-4, -4}, 0x89},
                        {{-4, -4}, 0x89},
                        {{-4, -4}, 0x8A},
                        {{-4, -4}, 0x8A},
                        {{-4, -4}, 0x8A},
                        {{-4, -4}, 0x8B},
                        {{-4, -4}, 0x8B},
                        {{-4, -4}, 0x8B},
                };
                const Sprite* first = sheet + state;
                Sprite meta{geom::top_left(body.box), 0};
                push_sprites(context.sprites, first, first + 1, meta);
        }
};

Mob* make_mob(Mob_place& place)
{
        switch (place.type) {
         case Mob_type::rotten: return new Rotten(place.box);
         case Mob_type::bones: return new Bones(place.box);
         case Mob_type::peeper: return new Peeper(place.box);
         case Mob_type::crawler: return new Crawler(place.box);
         case Mob_type::crack: return new Crack(place);
         case Mob_type::mirror: return new Mirror(place.box);
         case Mob_type::thorns: return new Thorns(place.box);
         default: return nullptr;
        }
}

// Generates a dust cloud
void dust(Context& context, points32v p)
{
        context.mobs.emplace_back(new Dust{p});
}

// Generates an explosion
void explode(points32v p, Context& context)
{
        context.mobs.emplace_back(new Explosion(p));
}

// Generates an splatter
void splat(points32v p, Context& context)
{
        context.mobs.emplace_back(new Splatter(p));
}

// Generates a bone projectile
void throw_bone(points32v p, Context& context, points32 vel_x)
{
        context.mobs.emplace_back(new Bone(p, vel_x));
}

// Spawns coins
void spawn_coins(points32v p, Context& context, int n)
{
        auto x = -4_pt * (n - 1);
        for (int i = 0; i < n; ++i) {
                context.mobs.emplace_back(new Coin(p, x));
                x += 8_pt;
        }
}

