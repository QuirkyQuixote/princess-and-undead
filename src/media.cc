// media.cc - main loog and media resource loaders

// Copyright (C) 2021-2023 L. Sanz <luis.sanz@gmail.com>

// This file is part of Princess and Undead.

// Princess and Undead is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Princess and Undead is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Princess and Undead.  If not, see <https://www.gnu.org/licenses/>.


#include "media.h"

#include <cstring>
#include <fstream>
#include <iostream>
#include <string>

#include "SDL_image.h"
#include "xdgbds.h"

Fps_manager::Fps_manager(int fps) : fps{fps}
{
        if (fps < 1 || fps > 200)
                throw std::runtime_error{"FPS out of range: " + std::to_string(fps)};
        t0 = SDL_GetTicks();
        t1 = t0;
        frame_count = 0;
}

int Fps_manager::sync()
{
        ++frame_count;
        int ticks = SDL_GetTicks();
        int time_elapsed = ticks - t1;
        t1 = ticks;
        int target_ticks = t0 + (frame_count * 1000) / fps;
        if (ticks <= target_ticks) {
                SDL_Delay(target_ticks - ticks);
        } else {
                t0 = t1;
                frame_count = 0;
        }
        return time_elapsed;
}

std::vector<Gamepad> open_all_gamepads()
{
        std::vector<Gamepad> solution;
        for (int i = 0; i < SDL_NumJoysticks(); ++i) {
                if (SDL_IsGameController(i)) {
                        auto gamepad = SDL_GameControllerOpen(i);
                        if (gamepad) solution.emplace_back(gamepad);
                        else std::cerr << SDL_GetError() << "\n";
                }
        }
        return solution;
}

std::vector<Joystick> open_all_joysticks()
{
        std::vector<Joystick> solution;
        for (int i = 0; i < SDL_NumJoysticks(); ++i) {
                if (!SDL_IsGameController(i)) {
                        auto joystick = SDL_JoystickOpen(i);
                        if (joystick) solution.emplace_back(joystick);
                        else std::cerr << SDL_GetError() << "\n";
                }
        }
        return solution;
}

void load_image_impl(GLenum type, const std::filesystem::path& path)
{
        Surface raw{IMG_Load(path.string().c_str())};
        if (!raw) throw std::runtime_error{IMG_GetError()};
        SDL_SetColorKey(raw.get(), SDL_TRUE, 0);
        Surface image{SDL_ConvertSurfaceFormat(raw.get(), SDL_PIXELFORMAT_RGBA32, 0)};
        if (!image) throw std::runtime_error{SDL_GetError()};
        auto w = image.get()->w;
        auto h = image.get()->h;
        auto p = image.get()->pixels;
        glTexImage2D(type, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, p);
}

Texture load_image(const std::filesystem::path& path)
{
        Texture texture;
        glBindTexture(GL_TEXTURE_2D, *texture);
        load_image_impl(GL_TEXTURE_2D, path);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        return texture;
}

Texture load_cubemap(const std::filesystem::path& right,
                const std::filesystem::path& left,
                const std::filesystem::path& top,
                const std::filesystem::path& bottom,
                const std::filesystem::path& front,
                const std::filesystem::path& back)
{
        Texture texture;
        glBindTexture(GL_TEXTURE_CUBE_MAP, *texture);
        load_image_impl(GL_TEXTURE_CUBE_MAP_POSITIVE_X, right);
        load_image_impl(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, left);
        load_image_impl(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, top);
        load_image_impl(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, bottom);
        load_image_impl(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, front);
        load_image_impl(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, back);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP);
        return texture;
}

Chunk load_sound(const std::filesystem::path& path)
{
        auto data = Mix_LoadWAV(path.string().c_str());
        if (data == NULL) throw std::runtime_error{Mix_GetError()};
        return Chunk(data);
}

Music load_music(const std::filesystem::path& path)
{
        auto data = Mix_LoadMUS(path.string().c_str());
        if (data == NULL) throw std::runtime_error{Mix_GetError()};
        return Music(data);
}

Shader::Shader(const char* code, GLenum type)
{
        n = glCreateShader(type);
        glShaderSource(n, 1, &code, NULL);
        glCompileShader(n);
        GLint len;
        glGetShaderiv(n, GL_INFO_LOG_LENGTH, &len);
        if (len > 0) {
                char buf[len];
                glGetShaderInfoLog(n, len, NULL, buf);
                std::cerr << buf;
        }
        GLint success;
        glGetShaderiv(n, GL_COMPILE_STATUS, &success);
        if (success != GL_TRUE)
                throw std::runtime_error{"Can't compile shader"};
}

Program::Program(std::initializer_list<Shader> shaders)
{
        n = glCreateProgram();
        for (auto& shader : shaders)
                glAttachShader(n, *shader);
        glLinkProgram(n);
        GLint len;
        glGetProgramiv(n, GL_INFO_LOG_LENGTH, &len);
        if (len > 0) {
                char buf[len];
                glGetProgramInfoLog(n, len, NULL, buf);
                std::cerr << buf;
        }
        GLint success;
        glGetProgramiv(n, GL_LINK_STATUS, &success);
        if (success != GL_TRUE)
                throw std::runtime_error{"Can't link program"};
}

std::string slurp(const std::filesystem::path& path)
{
        if (std::ifstream file{path.string()})
                return std::string(std::istreambuf_iterator<char>(file),
                                std::istreambuf_iterator<char>());
        throw std::runtime_error{path.string() + ": " + strerror(errno)};
}
