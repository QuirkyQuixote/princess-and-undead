// princess.h - declarations for player object

// Copyright (C) 2021-2023 L. Sanz <luis.sanz@gmail.com>

// This file is part of Princess and Undead.

// Princess and Undead is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Princess and Undead is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Princess and Undead.  If not, see <https://www.gnu.org/licenses/>.


#ifndef SRC_PRINCESS_H_
#define SRC_PRINCESS_H_

#include "physics.h"

// Forward declaration
struct Context;

// Data to define the princess herself.

struct Princess {
        Body body = {{}, {}, {}, 0};
        points32b attack_box;
        uint16_t flags = 0;
        int8_t subframe = 0;
        int8_t frame = 0;
        int8_t jump_counter = 0;
        int8_t attack_counter = 0;
        int8_t damage_counter = 0;
        int8_t health = 127;
        int8_t rest_counter = 0;
};

// Call to update the princess 
void update_princess(Context& context);

// Call to try to cause damage to the princess
bool harm_princess(Context& context, const points32b& box);

// Call to check if the princess can jump on the given box
bool bounce_princess(Context& context, const points32b& box);

#endif // SRC_PRINCESS_H_
