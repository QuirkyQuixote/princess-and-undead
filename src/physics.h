// physics.h - declare Body and Hitmask types

// Copyright (C) 2021-2023 L. Sanz <luis.sanz@gmail.com>

// This file is part of Princess and Undead.

// Princess and Undead is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Princess and Undead is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Princess and Undead.  If not, see <https://www.gnu.org/licenses/>.


#ifndef SRC_PHYSICS_H_
#define SRC_PHYSICS_H_

#include "geom/packed_table.h"

#include "space.h"

// A body has a shape, velocity, and some flags
struct Body {
        points32b box;          // an axis-aligned box
        points16v vel;          // current velocity
        boolb contact;          // contact points.
        bool bouncy : 1;        // bounces around
};

// A hitmask defines collision with the scene
using Hitmask = geom::Packed_table<cells16v, uint8_t>;

// call once per frame to move the box of a body by its velocity,
// unless it collides with the hitmask, in which case the movement will
// stop at the right point, and the velocity in that coordinate will
// either be set to zero (if the body is not bouncy) or be divided by
// -2 (if the body is bouncy).
void move_body(Body& body, Hitmask& hitmask);

#endif  // SRC_PHYSICS_H_
