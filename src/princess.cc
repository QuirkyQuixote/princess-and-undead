// princess.cc - logic for the player object

// Copyright (C) 2021-2023 L. Sanz <luis.sanz@gmail.com>

// This file is part of Princess and Undead.

// Princess and Undead is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Princess and Undead is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Princess and Undead.  If not, see <https://www.gnu.org/licenses/>.


#include "princess.h"

#include "context.h"
#include "mobs.h"

static const auto acc_x_ground = 2_pt;          // horizontal acceleration on ground
static const auto dec_x_ground = 2_pt;          // horizontal deceleration on ground
static const auto acc_x_air = 1_pt;             // horizontal acceleration on air
static const auto imp_x_wall = 16_pt;           // horizontal impulse applied on wall jump
static const auto acc_y_jump = 32_pt;           // vertical acceleration on jumps
static const auto max_x_speed = 16_pt;          // maximum horizontal speed
static const auto gravity = 8_pt;               // vertical acceleration on air
static const auto max_sliding_speed = 32_pt;    // maximum y-speed when wall-sliding

static const int8_t ground_jump_frames{4};      // max. # of jump frames from floor
static const int8_t invincibility_frames{16};   // # of invincibility frames after damage
static const int8_t wall_jump_frames{2};        // max. # of jump frames from wall
static const int8_t attack_cooldown{8};         // # of idle frames after attack

void move_x(Princess& pr, Context& context);
void move_y(Princess& pr, Context& context);
void attack(Princess& pr, Context& context);
void physics(Princess& pr, Context& context);
void animate(Princess& pr, Context& context);
void render(Princess& pr, Context& context);
void recuperate(Princess& pr, Context& context);

void update_princess(Context& context)
{
        move_x(context.princess, context);
        move_y(context.princess, context);
        attack(context.princess, context);
        physics(context.princess, context);
        animate(context.princess, context);
        render(context.princess, context);
        recuperate(context.princess, context);
}

// When touching the floor, both acceleration and deceleration are 2
// When falling, acceleration is 1, and there's no deceleration

void move_x(Princess& pr, Context& context)
{
        if (context.input.left) {
                if (pr.body.contact.d) pr.body.vel.x -= acc_x_ground;
                else pr.body.vel.x -= acc_x_air;
                pr.flags = Sprite_flags::fliph;
        } else if (context.input.right) {
                if (pr.body.contact.d) pr.body.vel.x += acc_x_ground;
                else pr.body.vel.x += acc_x_air;
                pr.flags = 0;
        } else if (!pr.body.contact.d) {
                // nothing to do
        } else if (pr.body.vel.x > 0_pt) {
                pr.body.vel.x = std::max<points32>(pr.body.vel.x - dec_x_ground, 0_pt);
        } else if (pr.body.vel.x < 0_pt) {
                pr.body.vel.x = std::min<points32>(pr.body.vel.x + dec_x_ground, 0_pt);
        }
}

// The jump counter is -1 when ready to jump. It is set to this value when the
// jump button is released.

// When the button is pressed, the counter is -1, and the princess is touching
// a top, left, or right surface, the counter is set to 4 (for regular jumps)
// or to 2 (for wall jumps), then starts counting down to zero each frame.

// On wall jumps, an additional impulse away from the wall is applied.

// While the jump counter is greater than zero, the vertical velocity is fixed
// to -32 (so vertical jumps allow gaining more height than wall jumps)
// else, if not standing on the floor, increase vertical velocity by 8.

void move_y(Princess& pr, Context& context)
{
        if (context.input.jump) {
                if (pr.jump_counter == -1) {
                        if (pr.body.contact.d) {
                                pr.jump_counter = ground_jump_frames;
                                dust(context, bottom(pr.body.box));
                        } else if (pr.body.contact.l) {
                                pr.body.vel.x = -imp_x_wall;
                                pr.jump_counter = wall_jump_frames;
                        } else if (pr.body.contact.r) {
                                pr.body.vel.x = imp_x_wall;
                                pr.jump_counter = wall_jump_frames;
                        }
                        if (pr.jump_counter > 0)
                                context.sounds[Sound::jump] = true;
                } else if (pr.jump_counter > 0) {
                        --pr.jump_counter;
                }
        } else {
                pr.jump_counter = -1;
        }
        if (pr.jump_counter > 0)
                pr.body.vel.y = -acc_y_jump;
        else if (!pr.body.contact.d)
                pr.body.vel.y += gravity;
        if ((pr.body.contact.l || pr.body.contact.r) && pr.body.vel.y > max_sliding_speed)
                pr.body.vel.y = max_sliding_speed;
}

// - The princess is ready to attack when the attack counter is 0.
// - Pressing the attack button when the counter is 0 sets it to -1.
// - Releasing the attack button when the counter is -1 sets the counter to 4.
//
// When the counter is greater than 0, the princess does not accept attack
// input, and it decreases by 1 each frame. While this happens, the princess
// is attacking.

// The first frame of the attack, the attack hitbox is set to a value other
// than (0,0,0,0).  Mobs that can be damaged test their own boxes against this
// one during their own update stage.

// Pressing UP while attacking makes the princess swing her ax around instead
// of thrusting it forwards.  Both the movement lines and the hitbox for the
// attack are different.

void attack(Princess& pr, Context& context)
{
        if (pr.attack_counter == 0) {
                if (context.input.attack)
                        pr.attack_counter = -1;
        } else if (pr.attack_counter == -1) {
                if (!context.input.attack) {
                        pr.attack_counter = attack_cooldown;
                        context.sounds[Sound::swing] = true;
                }
        } else {
                --pr.attack_counter;
        }
        if (pr.body.contact.d && pr.attack_counter != 0)
                pr.body.vel.x = 0_pt;
        if (pr.attack_counter != attack_cooldown)
                pr.attack_box = {0, 0, 0, 0};
        else if (context.input.up)
                pr.attack_box = points32b(-128, 128, -128, 0) + geom::center(pr.body.box);
        else if (pr.flags == Sprite_flags::fliph)
                pr.attack_box = points32b(-160, 0, -32, 32) + geom::center(pr.body.box);
        else
                pr.attack_box = points32b(0, 160, -32, 32) + geom::center(pr.body.box);
}

// Horizontal speed is clamped to in absolute value of 16.

// If the "top" flag of the body was false before the physics update, and true
// after, the princess has landed. Play the appropriate sound.

void physics(Princess& pr, Context& context)
{
        pr.body.vel.x = std::max<points32>(pr.body.vel.x, -max_x_speed);
        pr.body.vel.x = std::min<points32>(pr.body.vel.x, max_x_speed);
        bool resting = pr.body.contact.d;
        move_body(pr.body, context.hitmask);
        if (pr.body.contact.d && !resting) {
                context.sounds[Sound::land] = true;
                dust(context, bottom(pr.body.box));
        }
}

// Update the meta sprite frame.  Just read the code.
void animate(Princess& pr, Context& context)
{
        if (pr.body.contact.d) {
                // resting in the floor 
                if (pr.attack_counter == -1)
                        pr.frame = 12;     // preparing attack
                else if (pr.attack_counter > 0)
                        pr.frame = 13;     // attacking
                else if (pr.frame >= 18 && pr.frame <= 20)
                        pr.frame = 10;     // landing
                else if (pr.body.vel.x == 0_pt)
                        pr.frame = (pr.health == 127) ? 0 : (context.frame & 8) ? 21 : 22;      // idle
                else if ((pr.body.vel.x > 0_pt) == (pr.flags == Sprite_flags::fliph))
                        pr.frame = 14;     // brake
                else if (pr.frame < 5 || pr.frame >= 9)
                        pr.subframe = 0, pr.frame = 5;      // start running animation
                else if ((pr.subframe = (pr.subframe + 1) % 2) == 0)
                        ++pr.frame;        // advance running animation
        } else {
                // falling
                if (pr.damage_counter == invincibility_frames)
                        pr.frame = 17;     // hurt
                else if (pr.attack_counter == -1)
                        pr.frame = 15;     // preparing attack
                else if (pr.attack_counter > 0)
                        pr.frame = 16;     // attacking
                else if (pr.body.contact.l)
                        pr.frame = pr.flags & Sprite_flags::fliph ? 23 : 24;
                else if (pr.body.contact.r)
                        pr.frame = pr.flags & Sprite_flags::fliph ? 24 : 23;
                else if (pr.body.vel.y < -8_pt)
                        pr.frame = 18;     // falling
                else if (pr.body.vel.y < 8_pt)
                        pr.frame = 19;     // falling
                else
                        pr.frame = 20;     // falling
        }
        // Play step sound on step frames
        if (pr.frame == 7 && pr.subframe == 0)
                context.sounds[Sound::step] = true;
        // Add dust particles when wall-sliding
        if (!pr.body.contact.d) {
                if (pr.body.contact.l)
                        dust(context, bottom_right(pr.body.box));
                if (pr.body.contact.r)
                        dust(context, bottom_left(pr.body.box));
        // Add dust particles when braking
        } else {
                if (pr.body.vel.x > 8_pt && (pr.flags & Sprite_flags::fliph))
                        dust(context, bottom_right(pr.body.box));
                if (pr.body.vel.x < -8_pt && !(pr.flags & Sprite_flags::fliph))
                        dust(context, bottom_left(pr.body.box));
        }
}

// The princess meta sprite is rendered unless the invincibility counter after
// damage is active and this is an even frame.

// If this is the first frame after releasing the attack button, draw the
// movement lines for that particular attack.  The attack type is determined by
// the value of the flag for the up button.

void render(Princess& pr, Context& context)
{
        static const Sprite sheet[] = {
                {{-4, -10}, 0x00}, {{-4, -2}, 0x01}, {{-4, -2}, 0x0F}, {{4, -2}, 0x10}, // idle
                {{-4, -10}, 0x00}, {{-4, -2}, 0x02}, {{-4, -2}, 0x0F}, {{4, -2}, 0x10}, // walk #1
                {{-4, -9}, 0x00}, {{-4, -2}, 0x03}, {{-4, -1}, 0x0F}, {{4, -1}, 0x10}, // walk #2
                {{-4, -10}, 0x00}, {{-4, -2}, 0x04}, {{-4, -2}, 0x0F}, {{4, -2}, 0x10}, // walk #3
                {{-4, -10}, 0x00}, {{-4, -2}, 0x05}, {{-4, -2}, 0x0F}, {{4, -2}, 0x10}, // walk #4
                {{-4, -9}, 0x00}, {{-4, -2}, 0x06}, {{-4, -2}, 0x0F}, {{4, -2}, 0x10}, // run #1
                {{-4, -8}, 0x00}, {{-4, -2}, 0x07}, {{-4, -1}, 0x0F}, {{4, -1}, 0x10}, // run #2
                {{-4, -9}, 0x00}, {{-4, -2}, 0x08}, {{-4, -2}, 0x0F}, {{4, -2}, 0x10}, // run #3
                {{-4, -10}, 0x00}, {{-4, -2}, 0x09}, {{-4, -3}, 0x0F}, {{4, -3}, 0x10}, // run #4
                {{-4, -10}, 0x00}, {{-4, -2}, 0x0A}, {{-4, -3}, 0x0F}, {{4, -3}, 0x10}, // run #5
                {{-4, -8}, 0x00}, {{-4, -2}, 0x0B}, {{-4, 0}, 0x0F}, {{4, 0}, 0x10}, // crouch
                {{-4, -8}, 0x1D}, {{-4, -2}, 0x0C}, {{-4, -1}, 0x0F}, {{4, -1}, 0x10}, // hurt
                {{-4, -9}, 0x00}, {{-4, -2}, 0x0D}, {{-5, -3}, 0x0F}, {{3, -3}, 0x10}, // attack #1
                {{-3, -9}, 0x00}, {{-4, -2}, 0x0E}, {{1, -3}, 0x0F}, {{9, -3}, 0x10}, // attack #2
                {{-5, -9}, 0x00}, {{-4, -2}, 0x11}, {{-4, -2}, 0x0F}, {{4, -2}, 0x10}, // brake
                {{-4, -9}, 0x00}, {{-4, -2}, 0x12}, {{-5, -3}, 0x0F}, {{3, -3}, 0x10}, // attack #1
                {{-3, -9}, 0x00}, {{-4, -2}, 0x13}, {{1, -3}, 0x0F}, {{9, -3}, 0x10}, // attack #2
                {{-4, -10}, 0x1E}, {{-4, -2}, 0x1F}, {{-4, -2}, 0x0F}, {{4, -2}, 0x10}, // harmed
                {{-4, -10}, 0x00}, {{-4, -2}, 0x1A}, {{-4, -1}, 0x0F}, {{4, -1}, 0x10}, // jump
                {{-4, -10}, 0x00}, {{-4, -2}, 0x1B}, {{-4, -2}, 0x0F}, {{4, -2}, 0x10}, // jump
                {{-4, -10}, 0x00}, {{-4, -2}, 0x1C}, {{-4, -3}, 0x0F}, {{4, -3}, 0x10}, // jump
                {{-3, -7}, 0x00}, {{-4, -2}, 0x0C}, {{-4, -1}, 0x0F}, {{4, -1}, 0x10}, // hurt
                {{-4, -8}, 0x1D}, {{-4, -2}, 0x0C}, {{-4, -1}, 0x0F}, {{4, -1}, 0x10}, // hurt
                {{-2, -8}, 0x00}, {{-4, -2}, 0x3E}, {{-4, -1}, 0x0F}, {{4, -1}, 0x10}, // back to wall
                {{-4, -10}, 0x00}, {{-4, -2}, 0x3F}, {{-5, -4}, 0x0F}, {{3, -4}, 0x10}, // front to wall
        };

        static const Sprite swing_sheet[] = {
                {{-16, -8}, 0x34}, {{-16, -16}, 0x14}, {{-8, -16}, 0x15},
                {{0, -16}, 0x16}, {{8, -16}, 0x17}, {{8, -8}, 0x37}
        };

        static const Sprite thrust_sheet[] = {
                {{6, -3}, 0x18}, {{14, -3}, 0x19}
        };

        Sprite meta{geom::center(pr.body.box), pr.flags};
        if (pr.damage_counter == 0 || pr.damage_counter == invincibility_frames || context.frame % 2 == 0) {
                const Sprite* first = sheet + pr.frame * 4;
                push_sprites(context.sprites, first, first + 4, meta);
        }
        if (pr.attack_counter == attack_cooldown) {
                if (context.input.up)
                        push_sprites(context.sprites, swing_sheet, swing_sheet + 6, meta);
                else
                        push_sprites(context.sprites, thrust_sheet, thrust_sheet + 2, meta);
        }
}

// If the invincibility counter is positive, decrement it.
// If the princess is not standing on the floor or an input is pressed, reset
// the rest counter; otherwise decrement it.
// If the rest counter is zero, restore one health point.

void recuperate(Princess& pr, Context& context)
{
        if (pr.damage_counter > 0)
                --pr.damage_counter;
        if (!pr.body.contact.d || context.input.left || context.input.right
                        || context.input.attack || context.input.jump)
                pr.rest_counter = 64;
        else if (pr.rest_counter > 0)
                --pr.rest_counter;
        if (pr.rest_counter == 0 && pr.health < 127)
                ++pr.health;
}

// If the invincibility counter is zero and the princess's body box intersects
// with the given box, she takes damage: play the appropriate sound and reset
// the invincibility counter.

bool harm_princess(Context& context, const points32b& box)
{
        Princess& pr = context.princess;
        if (pr.damage_counter > 0)
                return false;
        pr.damage_counter = invincibility_frames;
        if (geom::center(pr.body.box).x < geom::center(box).x) pr.body.vel.x = -max_x_speed;
        else pr.body.vel.x = max_x_speed;
        pr.body.contact.d = false; // prevent floor from stopping movement
        pr.body.vel.y = -acc_y_jump;
        pr.health -= 32;
        if (pr.health > 0) {
                context.sounds[Sound::hurt] = true;
                context.freeze = 4;
        } else {
                context.sounds[Sound::death] = true;
                context.freeze = 64;
        }
        return true;
}

// If the princess collides with a mob from the top and presses the jump button
// at exactly the right time, she can jump back up.

bool bounce_princess(Context& context, const points32b& box)
{
        Princess& pr = context.princess;
        points32 d = pr.body.box.u - box.d;
        if (d < pr.body.vel.y && context.input.jump && pr.jump_counter == -1) {
                pr.body.box.d -= d;
                pr.body.box.u -= d;
                pr.body.vel.y = 0_pt;
                pr.jump_counter = ground_jump_frames;
                context.sounds[Sound::bounce] = true;
                return true;
        }
        return false;
}
