// event.cc - implementation for event catchers

// Copyright (C) 2021-2023 L. Sanz <luis.sanz@gmail.com>

// This file is part of Princess and Undead.

// Princess and Undead is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Princess and Undead is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Princess and Undead.  If not, see <https://www.gnu.org/licenses/>.


#include "event.h"

#include <stdexcept>
#include <istream>
#include <ostream>

template<class T, std::enable_if_t<std::is_enum_v<T>, int> = 0>
std::ostream& operator<<(std::ostream& stream, const T& value)
{ return stream << reinterpret_cast<const std::underlying_type_t<T>&>(value); }

template<class T, std::enable_if_t<std::is_enum_v<T>, int> = 0>
std::istream& operator>>(std::istream& stream, T& value)
{ return stream >> reinterpret_cast<std::underlying_type_t<T>&>(value); }

// Visitor for event catching.
//
// Holds a reference to the event being tested, and the original state of the
// value, and implements operator() for every possible catcher type

struct Catch_event {
        const SDL_Event& event;
        bool old_state;

        bool operator()(const Keycode_catcher& c)
        {
                const auto& e = event.key;
                return (e.type == SDL_KEYDOWN && e.keysym.sym == c.code) ? true :
                       (e.type == SDL_KEYUP && e.keysym.sym == c.code) ? false :
                        old_state;
        }

        bool operator()(const Scancode_catcher& c)
        {
                const auto& e = event.key;
                return (e.type == SDL_KEYDOWN && e.keysym.scancode == c.code) ? true :
                       (e.type == SDL_KEYUP && e.keysym.scancode == c.code) ? false :
                       old_state;
        }

        bool operator()(const Mouse_button_catcher& c)
        {
                const auto& e = event.button;
                return (e.type == SDL_MOUSEBUTTONDOWN && e.which == c.which && e.button == c.button) ? true :
                       (e.type == SDL_MOUSEBUTTONUP && e.which == c.which && e.button == c.button) ? false :
                       old_state;
        }

        bool operator()(const Controller_button_catcher& c)
        {
                const auto& e = event.cbutton;
                return (e.type == SDL_CONTROLLERBUTTONDOWN && e.which == c.which && e.button == c.button) ? true :
                       (e.type == SDL_CONTROLLERBUTTONUP && e.which == c.which && e.button == c.button) ? false :
                       old_state;
        }

        bool operator()(const Controller_axis_n_catcher& c)
        {
                const auto& e = event.caxis;
                if (e.type == SDL_CONTROLLERAXISMOTION && e.which == c.which && e.axis == c.axis)
                        return e.value < -16000;
                return old_state;
        }

        bool operator()(const Controller_axis_p_catcher& c)
        {
                const auto& e = event.caxis;
                if (e.type == SDL_CONTROLLERAXISMOTION && e.which == c.which && e.axis == c.axis)
                        return e.value > 16000;
                return old_state;
        }

        bool operator()(const Joystick_button_catcher& c)
        {
                const auto& e = event.jbutton;
                return (e.type == SDL_JOYBUTTONDOWN && e.which == c.which && e.button == c.button) ? true :
                       (e.type == SDL_JOYBUTTONUP && e.which == c.which && e.button == c.button) ? false :
                       old_state;
        }

        bool operator()(const Joystick_axis_n_catcher& c)
        {
                const auto& e = event.jaxis;
                if (e.type == SDL_JOYAXISMOTION && e.which == c.which && e.axis == c.axis)
                        return e.value < -16000;
                return old_state;
        }

        bool operator()(const Joystick_axis_p_catcher& c)
        {
                const auto& e = event.jaxis;
                if (e.type == SDL_JOYAXISMOTION && e.which == c.which && e.axis == c.axis)
                        return e.value > 16000;
                return old_state;
        }

        bool operator()(const Joystick_hat_catcher& c)
        {
                const auto& e = event.jhat;
                if (e.type == SDL_JOYHATMOTION && e.which == c.which && e.hat == c.hat)
                        return e.value == c.value;
                return old_state;
        }

        bool operator()(const Quit_catcher& c)
        {
                if (event.type == SDL_QUIT)
                        return true;
                return old_state;
        }

};

bool catch_event_fn::operator()(const SDL_Event& event,
                const Event_catcher& catcher, bool old_state) const
{ return std::visit(Catch_event{event, old_state}, catcher); }

// Visitor for event catcher printing
//
// Holds a reference to the output stream, and provides implementations of
// operator() for every possible event catcher.

struct Print_catcher {
        std::ostream& stream;

        std::ostream& operator()(const Scancode_catcher& c)
        { return stream << "scancode " << c.code; }

        std::ostream& operator()(const Keycode_catcher& c)
        { return stream << "keycode " << c.code; }

        std::ostream& operator()(const Mouse_button_catcher& c)
        { return stream << "mbutton " << c.which << " " << c.button; }

        std::ostream& operator()(const Controller_button_catcher& c)
        { return stream << "cbutton " << c.which << " " << c.button; }

        std::ostream& operator()(const Controller_axis_n_catcher& c)
        { return stream << "caxis- " << c.which << " " << c.axis; }

        std::ostream& operator()(const Controller_axis_p_catcher& c)
        { return stream << "caxis+ " << c.which << " " << c.axis; }

        std::ostream& operator()(const Joystick_button_catcher& c)
        { return stream << "jbutton " << c.which << " " << c.button; }

        std::ostream& operator()(const Joystick_axis_n_catcher& c)
        { return stream << "jaxis- " << c.which << " " << c.axis; }

        std::ostream& operator()(const Joystick_axis_p_catcher& c)
        { return stream << "jaxis+ " << c.which << " " << c.axis; }

        std::ostream& operator()(const Joystick_hat_catcher& c)
        { return stream << "jhat " << c.which << " " << c.hat << " " << c.value; }

        std::ostream& operator()(const Quit_catcher& c)
        { return stream << "quit"; }
};

std::ostream& operator<<(std::ostream& stream, const Event_catcher& catcher)
{ return std::visit(Print_catcher{stream}, catcher); }

// Visitor for event catcher scanning
//
// Holds a reference to the output stream, and provides implementations of
// operator() for every possible event catcher.

struct Scan_catcher {
        std::istream& stream;

        std::istream& operator()(Keycode_catcher& catcher)
        { return stream >> catcher.code; }

        std::istream& operator()(Scancode_catcher& catcher)
        { return stream >> catcher.code; }

        std::istream& operator()(Mouse_button_catcher& catcher)
        { return stream >> catcher.which >> catcher.button; }

        std::istream& operator()(Controller_button_catcher& catcher)
        { return stream >> catcher.which >> catcher.button; }

        std::istream& operator()(Controller_axis_n_catcher& catcher)
        { return stream >> catcher.which >> catcher.axis; }

        std::istream& operator()(Controller_axis_p_catcher& catcher)
        { return stream >> catcher.which >> catcher.axis; }

        std::istream& operator()(Joystick_button_catcher& catcher)
        { return stream >> catcher.which >> catcher.button; }

        std::istream& operator()(Joystick_axis_n_catcher& catcher)
        { return stream >> catcher.which >> catcher.axis; }

        std::istream& operator()(Joystick_axis_p_catcher& catcher)
        { return stream >> catcher.which >> catcher.axis; }

        std::istream& operator()(Joystick_hat_catcher& catcher)
        { return stream >> catcher.which >> catcher.hat >> catcher.value; }

        std::istream& operator()(Quit_catcher& catcher)
        { return stream; }
};

std::istream& operator>>(std::istream& stream, Event_catcher& catcher)
{
        std::string tok;
        stream >> tok;
        if (tok == "keycode") catcher = Keycode_catcher{};
        else if (tok == "scancode") catcher = Scancode_catcher{};
        else if (tok == "mbutton") catcher = Mouse_button_catcher{};
        else if (tok == "cbutton") catcher = Controller_button_catcher{};
        else if (tok == "caxis-") catcher = Controller_axis_n_catcher{};
        else if (tok == "caxis+") catcher = Controller_axis_p_catcher{};
        else if (tok == "jbutton") catcher = Joystick_button_catcher{};
        else if (tok == "jaxis-") catcher = Joystick_axis_n_catcher{};
        else if (tok == "jaxis+") catcher = Joystick_axis_p_catcher{};
        else if (tok == "jhat") catcher = Joystick_hat_catcher{};
        else if (tok == "quit") catcher = Quit_catcher{};
        else throw std::runtime_error{tok + " is not a valid event catcher type"};
        return std::visit(Scan_catcher{stream}, catcher);
}

std::optional<Event_catcher> make_event_catcher(const SDL_Event& event)
{
        if (event.type == SDL_KEYUP)
                return Scancode_catcher{event.key.keysym.scancode};
        if (event.type == SDL_MOUSEBUTTONUP)
                return Mouse_button_catcher{(uint16_t)event.button.which, (uint16_t)event.button.button};
        if (event.type == SDL_CONTROLLERBUTTONUP)
                return Controller_button_catcher{(uint16_t)event.cbutton.which, (uint16_t)event.cbutton.button};
        if (event.type == SDL_CONTROLLERAXISMOTION && event.caxis.value == -32768)
                return Controller_axis_n_catcher{(uint16_t)event.caxis.which, (uint16_t)event.caxis.axis};
        if (event.type == SDL_CONTROLLERAXISMOTION && event.caxis.value == 32767)
                return Controller_axis_p_catcher{(uint16_t)event.caxis.which, (uint16_t)event.caxis.axis};
        if (event.type == SDL_JOYBUTTONUP)
                return Joystick_button_catcher{(uint16_t)event.jbutton.which, (uint16_t)event.jbutton.button};
        if (event.type == SDL_JOYAXISMOTION && event.jaxis.value == -32768)
                return Joystick_axis_n_catcher{(uint16_t)event.jaxis.which, (uint16_t)event.jaxis.axis};
        if (event.type == SDL_JOYAXISMOTION && event.jaxis.value == 32767)
                return Joystick_axis_p_catcher{(uint16_t)event.jaxis.which, (uint16_t)event.jaxis.axis};
        if (event.type == SDL_JOYHATMOTION)
                return Joystick_hat_catcher{(uint16_t)event.jhat.which, (uint16_t)event.jhat.hat, (uint16_t)event.jhat.value};
        return std::nullopt;
}
