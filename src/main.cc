// main.cc - application entrypoint

// Copyright (C) 2021-2023 L. Sanz <luis.sanz@gmail.com>

// This file is part of Princess and Undead.

// Princess and Undead is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Princess and Undead is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Princess and Undead.  If not, see <https://www.gnu.org/licenses/>.


#include <iostream>

#include "config.h"
#include "context.h"
#include "game.h"
#include "xdgbds.h"
#include "option.h"
#include "input.h"

const char usage_string[] =
"Usage: princess --help\n"
"       princess --version\n"
"       princess --config\n"
"       princess --input\n"
"       princess\n"
"\n";

const char option_string[] =
"Options:\n"
"  -h, --help           display this help and exit\n"
"  -v, --version        display version string and exit\n"
"  -c, --config         create configuration file\n"
"  -i, --input          query inputs and create configuration file\n"
"\n";

Config load_default_config()
{
        Config config;
        try {
                config = load_config(xdg::config::find(BASEDIR ".json"));
                config.window.fps = 30;
        } catch (std::exception& ex) {
                std::cerr << ex.what() << "\n";
                std::cerr << "Defaulting to hardcoded configuration.\n";
        }
        return config;
}

void save_default_config(const Config& config)
{
        auto path = xdg::config::home() / BASEDIR ".json";
        save_config(config, path);
        std::cout << "Config file saved to " << path.string() << "\n\n";
}

void parse_options(char** first, char** last)
{
        option::Option_parser option_parser = {
                { 'h', "help", false },
                { 'v', "version", false },
                { 'c', "config", false },
                { 'i', "input", false },
        };

        for (auto opt : option_parser(first, last)) {
                if (opt.first.short_str == 'h') {
                        std::cout << usage_string;
                        std::cout << option_string;
                        exit(EXIT_SUCCESS);
                } else if (opt.first.short_str == 'v') {
                        std::cout << VERSION << "\n";
                        exit(EXIT_SUCCESS);
                } else if (opt.first.short_str == 'c') {
                        save_default_config(Config{});
                        exit(EXIT_SUCCESS);
                } else if (opt.first.short_str == 'i') {
                        Config config = load_default_config();
                        Query_inputs query_inputs;
                        config.input.left = query_inputs("left");
                        config.input.right = query_inputs("right");
                        config.input.up = query_inputs("up");
                        config.input.down = query_inputs("down");
                        config.input.jump = query_inputs("jump");
                        config.input.attack = query_inputs("attack");
                        save_default_config(config);
                        exit(EXIT_SUCCESS);
                }
        }
}

int main(int argc, char *argv[])
{
        parse_options(argv + 1, argv + argc);

        try {
                Context context = load_context(xdg::data::find(BASEDIR "/levels/untitled.json"));
                Update{load_default_config(), context}();
                return 0;
        } catch (std::exception& ex) {
                std::cerr << ex.what() << "\n";
                return -1;
        }
        return 0;
}
