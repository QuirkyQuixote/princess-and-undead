// media.h - main loop and SDL/OpenGL resource wrappers

// Copyright (C) 2021-2023 L. Sanz <luis.sanz@gmail.com>

// This file is part of Princess and Undead.

// Princess and Undead is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Princess and Undead is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Princess and Undead.  If not, see <https://www.gnu.org/licenses/>.


#ifndef SRC_MEDIA_H_
#define SRC_MEDIA_H_

#include <filesystem>
#include <memory>
#include <vector>

#include "SDL.h"
#include "SDL_mixer.h"
#include "GL/glew.h"
#include "glm/glm.hpp"

// Read file into a string

std::string slurp(const std::filesystem::path& path);

// Locks for managing SDL subsystems by RAII

template<Uint32 Flags> struct Subsystem_lock {
        Subsystem_lock()
        {
                if (SDL_InitSubSystem(Flags) != 0)
                        throw std::runtime_error{SDL_GetError()};
        }
        ~Subsystem_lock() { SDL_QuitSubSystem(Flags); }
};

// Wrappers for SDL objects
//
// These represent unique resources; they can be moved, but not copied.
// To use the actual object, the get() member function is required
// (automatically casting to the pointer type would short circuit
// the constructor, allowing multiple "unique" pointers to exist)

struct Window_deleter {
        void operator()(SDL_Window* p) const { SDL_DestroyWindow(p); }
};

using Window = std::unique_ptr<SDL_Window, Window_deleter>;

struct Opengl_context_deleter {
        void operator()(void* p) const { SDL_GL_DeleteContext(p); }
};

using Opengl_context = std::unique_ptr<void, Opengl_context_deleter>;

struct Surface_deleter {
        void operator()(SDL_Surface* p) const { SDL_FreeSurface(p); }
};

using Surface = std::unique_ptr<SDL_Surface, Surface_deleter>;

struct Joystick_deleter {
        void operator()(SDL_Joystick* p) const { SDL_JoystickClose(p); }
};

using Joystick = std::unique_ptr<SDL_Joystick, Joystick_deleter>;

struct Gamepad_deleter {
        void operator()(SDL_GameController* p) const { SDL_GameControllerClose(p); }
};

using Gamepad = std::unique_ptr<SDL_GameController, Gamepad_deleter>;

struct Chunk_deleter {
        void operator()(Mix_Chunk* p) const { Mix_FreeChunk(p); }
};

using Chunk = std::unique_ptr<Mix_Chunk, Chunk_deleter>;

struct Music_deleter {
        void operator()(Mix_Music* p) const { Mix_FreeMusic(p); }
};

using Music = std::unique_ptr<Mix_Music, Music_deleter>;

// Ensure that FPS do not exceed the given value by calling sync() at the end
// of every game loop.

class Fps_manager {
 private:
        int t0;
        int t1;
        int frame_count;
        int fps;

 public:
        explicit Fps_manager(int fps);
        int sync();
};

// Options for constructing the window and initializing graphics

struct Window_config {
        int width = -1;
        int height = -1;
        bool fullscreen = true;
        int vsync = 1;
        int fps = 30;
};

// There should be only one instance of Main_loop: constructing it will
// initialize SDL, and destroying it terminate it.

// Uses the Curiously Recurring Template Pattern, so it should be used as
// public base class by Impl.

template<class Impl> class Main_loop {
 public:
        Main_loop(const Window_config& config) : fps_manager{config.fps}
        {
                // Set up a window
                SDL_DisplayMode mode;
                SDL_GetDesktopDisplayMode(0, &mode);
                int flags = SDL_WINDOW_OPENGL;
                int width = config.width < 0 ? mode.w : config.width;
                int height = config.height < 0 ? mode.h : config.height;
                if (config.fullscreen) flags |= SDL_WINDOW_FULLSCREEN;
                window.reset(SDL_CreateWindow(BASEDIR, 0, 0, width, height, flags));
                if (!window) throw std::runtime_error{SDL_GetError()};

                // Set up an OpenGL context for the newly created window
                context.reset(SDL_GL_CreateContext(window.get()));
                if (!context) throw std::runtime_error{SDL_GetError()};
                SDL_GL_SetSwapInterval(config.vsync);

                GLenum glewError = glewInit();
                if (glewError != GLEW_OK)
                        throw std::runtime_error{(char*)glewGetErrorString(glewError)};
        }

        void operator()()
        {
                for (;;) {
                        SDL_GL_MakeCurrent(window.get(), context.get());
                        if (reinterpret_cast<Impl*>(this)->update()) break;
                        SDL_GL_SwapWindow(window.get());
                        fps_manager.sync();
                }
        }

 private:
        Subsystem_lock<SDL_INIT_VIDEO> slock;
        Window window;
        Opengl_context context;
        Fps_manager fps_manager;
};

inline std::pair<GLint, GLint> get_viewport()
{
        int width, height;
        SDL_GetWindowSize(SDL_GL_GetCurrentWindow(), &width, &height);
        return std::make_pair(width, height);
}

// Wrappers for OpenGL objects
//
// OpenGL does not represent objects by pointers, but by opaque index numbers;
// otherwise, we want them to behave as if they were unique pointers.

class Vertex_array {
 private:
        GLuint n = -1;

 public:
        Vertex_array() { glGenVertexArrays(1, &n); }
        ~Vertex_array() { glDeleteVertexArrays(1, &n); }

        constexpr Vertex_array(const Vertex_array&) = delete;
        constexpr Vertex_array(Vertex_array&& r) { std::swap(n, r.n); }
        constexpr Vertex_array& operator=(const Vertex_array&) = delete;
        constexpr Vertex_array& operator=(Vertex_array&& r) { std::swap(n, r.n); return *this; }
        constexpr GLuint get() const { return n; }
        constexpr GLuint operator*() const { return n; }
};

class Frame_buffer {
 private:
        GLuint n = -1;

 public:
        Frame_buffer() { glGenFramebuffers(1, &n); }
        ~Frame_buffer() { glDeleteFramebuffers(1, &n); }

        constexpr Frame_buffer(const Frame_buffer&) = delete;
        constexpr Frame_buffer(Frame_buffer&& r) { std::swap(n, r.n); }
        constexpr Frame_buffer& operator=(const Frame_buffer&) = delete;
        constexpr Frame_buffer& operator=(Frame_buffer&& r) { std::swap(n, r.n); return *this; }
        constexpr GLuint get() const { return n; }
        constexpr GLuint operator*() const { return n; }
};

class Texture {
 private:
        GLuint n = -1;

 public:
        Texture() { glGenTextures(1, &n); }
        ~Texture() { glDeleteTextures(1, &n); }

        constexpr Texture(const Texture&) = delete;
        constexpr Texture(Texture&& r) { std::swap(n, r.n); }
        constexpr Texture& operator=(const Texture&) = delete;
        constexpr Texture& operator=(Texture&& r) { std::swap(n, r.n); return *this; }
        constexpr GLuint get() const { return n; }
        constexpr GLuint operator*() const { return n; }
};

class Render_buffer {
 private:
        GLuint n = -1;

 public:
        Render_buffer() { glGenRenderbuffers(1, &n); }
        ~Render_buffer() { glDeleteRenderbuffers(1, &n); }

        constexpr Render_buffer(const Render_buffer&) = delete;
        constexpr Render_buffer(Render_buffer&& r) { std::swap(n, r.n); }
        constexpr Render_buffer& operator=(const Render_buffer&) = delete;
        constexpr Render_buffer& operator=(Render_buffer&& r) { std::swap(n, r.n); return *this; }
        constexpr GLuint get() const { return n; }
        constexpr GLuint operator*() const { return n; }
};

class Buffer {
 private:
        GLuint n = -1;

 public:
        Buffer() { glGenBuffers(1, &n); }
        ~Buffer() { glDeleteBuffers(1, &n); }

        constexpr Buffer(const Buffer&) = delete;
        constexpr Buffer(Buffer&& r) { std::swap(n, r.n); }
        constexpr Buffer& operator=(const Buffer&) = delete;
        constexpr Buffer& operator=(Buffer&& r) { std::swap(n, r.n); return *this; }
        constexpr GLuint get() const { return n; }
        constexpr GLuint operator*() const { return n; }
};

class Shader {
 private:
        GLuint n = -1;

 public:
        Shader(const std::filesystem::path& path, GLenum type)
                : Shader(slurp(path), type) {}
        Shader(const std::string& code, GLenum type)
                : Shader(code.data(), type) {}
        Shader(const char* code, GLenum type);
        ~Shader() { glDeleteShader(n); }

        constexpr Shader(const Shader&) = delete;
        constexpr Shader(Shader&& r) { std::swap(n, r.n); }
        constexpr Shader& operator=(const Shader&) = delete;
        constexpr Shader& operator=(Shader&& r) { std::swap(n, r.n); return *this; }
        constexpr GLuint get() const { return n; }
        constexpr GLuint operator*() const { return n; }
};

class Program {
 private:
        GLuint n = -1;

 public:
        Program(std::initializer_list<Shader> shaders);
        ~Program() { glDeleteProgram(n); }

        constexpr Program(const Program&) = delete;
        constexpr Program(Program&& r) { std::swap(n, r.n); }
        constexpr Program& operator=(const Program&) = delete;
        constexpr Program& operator=(Program&& r) { std::swap(n, r.n); return *this; }
        constexpr GLuint get() const { return n; }
        constexpr GLuint operator*() const { return n; }
};

template<class... Args>
Shader vertex_shader(Args&&... args)
{ return Shader(std::forward<Args>(args)..., GL_VERTEX_SHADER); }

template<class... Args>
Shader fragment_shader(Args&&... args)
{ return Shader(std::forward<Args>(args)..., GL_FRAGMENT_SHADER); }

// Open all connected gamepads or joysticks

std::vector<Gamepad> open_all_gamepads();
std::vector<Joystick> open_all_joysticks();

// Load sound from file.

Chunk load_sound(const std::filesystem::path& path);
Music load_music(const std::filesystem::path& path);

// Handler for uniform variables
//
// Once constructed from a program and the variable name, new values can be
// assigned with either the assign() function or operator=()

template<class T> class Uniform {
 private:
        GLint n = -1;

 public:
        Uniform(const Program& program, const char* name)
                : n{glGetUniformLocation(*program, name)} { }

        constexpr Uniform(const Uniform& r) : n{r.n} {}
        constexpr Uniform(Uniform&& r) : n{r.n} {}
        constexpr Uniform& operator=(const Uniform& r) { n = r.n; return *this; }
        constexpr Uniform& operator=(Uniform&& r) { n = r.n; return *this; }
        constexpr GLint get() const { return n; }
        constexpr GLint operator*() const { return n; }

        Uniform& operator=(const T& x)
        {
                if constexpr (std::same_as<T, GLint>)
                        glUniform1i(n, x);
                if constexpr (std::same_as<T, glm::ivec2>)
                        glUniform2iv(n, 1, &x[0]);
                if constexpr (std::same_as<T, glm::ivec3>)
                        glUniform3iv(n, 1, &x[0]);
                if constexpr (std::same_as<T, glm::ivec4>)
                        glUniform4iv(n, 1, &x[0]);
                if constexpr (std::same_as<T, GLfloat>)
                        glUniform1f(n, x);
                if constexpr (std::same_as<T, glm::vec2>)
                        glUniform2fv(n, 1, &x[0]);
                if constexpr (std::same_as<T, glm::vec3>)
                        glUniform3fv(n, 1, &x[0]);
                if constexpr (std::same_as<T, glm::vec4>)
                        glUniform4fv(n, 1, &x[0]);
                if constexpr (std::same_as<T, glm::mat2>)
                        glUniformMatrix2fv(n, 1, GL_FALSE, &x[0][0]);
                if constexpr (std::same_as<T, glm::mat3>)
                        glUniformMatrix3fv(n, 1, GL_FALSE, &x[0][0]);
                if constexpr (std::same_as<T, glm::mat4>)
                        glUniformMatrix4fv(n, 1, GL_FALSE, &x[0][0]);
                return *this;
        }
};

Texture load_image(const std::filesystem::path& path);

Texture load_cubemap(const std::filesystem::path& right,
                const std::filesystem::path& left,
                const std::filesystem::path& top,
                const std::filesystem::path& bottom,
                const std::filesystem::path& front,
                const std::filesystem::path& back);

#endif  // SRC_MEDIA_H_
