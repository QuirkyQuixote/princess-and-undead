// input.h - interactive input configuration

// Copyright (C) 2021-2023 L. Sanz <luis.sanz@gmail.com>

// This file is part of Princess and Undead.

// Princess and Undead is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Princess and Undead is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Princess and Undead.  If not, see <https://www.gnu.org/licenses/>.

#ifndef SRC_INPUT_H_
#define SRC_INPUT_H_

#include <iostream>
#include <algorithm>

#include "config.h"
#include "media.h"

class Query_inputs {
 private:
        Subsystem_lock<SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER> slock;
        Window window{SDL_CreateWindow(BASEDIR, 0, 0, 100, 100, 0)};
        std::vector<Gamepad> gamepads = open_all_gamepads();

        Event_catcher find_catchable_event()
        {
                SDL_Event event;
                while (SDL_WaitEvent(&event)) {
                        if (event.type == SDL_QUIT)
                                exit(EXIT_SUCCESS);
                        if (event.type != SDL_KEYUP &&
                            event.type != SDL_CONTROLLERBUTTONUP &&
                            event.type != SDL_CONTROLLERAXISMOTION)
                                continue;
                        if (auto catcher = make_event_catcher(event))
                                return *catcher;
                }
                throw std::runtime_error{SDL_GetError()};
        }

 public:
        std::vector<Event_catcher> operator()(const std::string_view& name)
        {
                std::cout << "Press buttons for " << name << " (repeat to end)\n";
                std::vector<Event_catcher> r;
                for (;;) {
                        Event_catcher catcher = find_catchable_event();
                        if (std::find(r.begin(), r.end(), catcher) != r.end())
                                return r;
                        std::cout << "  " << catcher << "\n";
                        r.push_back(catcher);
                }
        }
};

#endif // SRC_INPUT_H_
