// render.h - declarations for scene rendering

// Copyright (C) 2021-2023 L. Sanz <luis.sanz@gmail.com>

// This file is part of Princess and Undead.

// Princess and Undead is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Princess and Undead is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Princess and Undead.  If not, see <https://www.gnu.org/licenses/>.


#ifndef SRC_RENDER_H_
#define SRC_RENDER_H_

#include <vector>

#include "glm/glm.hpp"
#include "geom/packed_table.h"
#include "xdgbds.h"

#include "space.h"
#include "media.h"
#include "config.h"

// Simple vertex for 2D drawing
struct Vertex {
        GLfloat x, y;   // position
        GLfloat u, v;   // texture coordinates
};

// Holds an OpenGL vertex array and vertex buffer.
// The array contents can be modified by calling update().
// The array is rendered by calling render().

class Geometry {
 private:
        Vertex_array vao_;
        Buffer vbo_;
        size_t size_{0};

 public:
        Geometry();
        void update(const Vertex* buf, size_t len);
        void render();
};

// The nametable contains the tiles for the background layer.
using Nametable = geom::Packed_table<cells16v, uint16_t>;

// Manages the background layer, rendered from an array of indices that refer
// to 8x8 elements in the texture of the vertex array.

// The nametable is an array of indices of texture elements, plus some flags.
// It is interpreted as a two-dimensional table of width stride.

// Call update() after modifying the tile table or its stride.
// Call render() to render the current contents of the vertex array.

struct Tile_layer {
        Geometry geometry;
        Texture texture = load_image(xdg::data::find(BASEDIR "/textures/tiles.gif"));

        void update(const Nametable& nametable);
        void render();
};

// Sprites are free-moving elements that are drawn to the screen
struct Sprite {
        pixels16v p;    // position
        uint16_t c;      // index of the texture element plus flags
};

// Bits beyond the eighth in a sprite index are flags.
enum Sprite_flags : uint16_t {
        pal0 = 0x0000,          // Use palette #0
        pal1 = 0x0100,          // Use palette #1
        pal2 = 0x0200,          // Use palette #2
        pal3 = 0x0300,          // Use palette #3
        fliph = 0x0400,         // Flip texture horizontally
        flipv = 0x0800,         // Flip texture vertically
};

// Manages the sprite layer, rendered from freely moving 8x8 elements from the
// texture in the vertex array.

// Call update() after modifying the sprite list.
// Call render() to render the current contents of the vertex array.

struct Sprite_layer {
        Geometry geometry;
        Texture texture = load_image(xdg::data::find(BASEDIR "/textures/sprites.gif"));

        void update(const std::vector<Sprite>& sprites);
        void render();
};

// Build data in a sprite list on the fly.
//
// A sprite list consists on a range of sprites, plus a meta-sprite. Each of
// the sprites are transformed in one sprite in the final sprite list, and the
// metasprite determines how the transformation happens.
//
// If the metasprite is flipped, it not only toggles the flip bits of the
// individual sprites, but flips their position in relation to zero.
//
// The updated position for each sprite is then added to the position of the
// metasprite to generate the value that is pushed to the list.

template<class I>
void push_sprites(std::vector<Sprite>& sprites, I begin, I end, Sprite meta)
{
        while (begin != end) {
                auto sprite = *begin++;
                sprite.c ^= meta.c;
                if (meta.c & Sprite_flags::fliph)
                        sprite.p.x = -(sprite.p.x + 8_px);
                if (meta.c & Sprite_flags::flipv)
                        sprite.p.y = -(sprite.p.y + 8_px);
                sprite.p += meta.p;
                sprites.push_back(sprite);
        }
}

// Push vertices for the given sprite
//
// Each sprite will push six vertices (two triangles) to the vertex list. Their
// positions depend entirely on the sprite position. All sprites are the same
// size (8x8)
//
// The texture coordinates are determined by the texture element
// data and they may be flipped with the appropriate Sprite_flags values.

void push_vertices(std::vector<Vertex>& vertices, Sprite sprite);

#endif          // SRC_RENDER_H_
