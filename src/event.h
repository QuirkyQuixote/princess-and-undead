// event.h - declare event catchers

// Copyright (C) 2021-2023 L. Sanz <luis.sanz@gmail.com>

// This file is part of Princess and Undead.

// Princess and Undead is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Princess and Undead is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Princess and Undead.  If not, see <https://www.gnu.org/licenses/>.


#ifndef SRC_EVENT_H_
#define SRC_EVENT_H_

#include <variant>
#include <optional>
#include <vector>
#include <ostream>

#include "SDL_events.h"

// Event catchers can map events to input element states.
//
// Each catcher type handles one (or more, for those events that have an
// up/down variant) type of event, and set up a boolean variable that
// determines if the player is trying to set that input on or off.
//
// For events that operate in a range of values other than true/false, a
// threshold value is given that determines if the input is on or off.
//
// Keycode_catcher              keyboard event after layout transformation
// Scancode_catcher             keyboard event before layout transformation
// Mouse_button_catcher         mouse button event
// Controller_button_catcher    Game controller button after mapping
// Controller_axis_n_catcher    Game controller axis after mapping
// Controller_axis_p_catcher    Game controller axis after mapping
// Joystick_button_catcher      Joystick button before mapping
// Joystick_axis_n_catcher      Joystick axis before mapping
// Joystick_axis_p_catcher      Joystick axis before mapping
// Joystick_hat_catcher         Joystick hat before mapping
// Quit_catcher                 quit event

struct Keycode_catcher {
        SDL_Keycode code;
        bool operator==(const Keycode_catcher& r) const { return code == r.code; }
};

struct Scancode_catcher {
        SDL_Scancode code;
        bool operator==(const Scancode_catcher& r) const { return code == r.code; }
};

struct Mouse_button_catcher {
        uint16_t which;
        uint16_t button;
        bool operator==(const Mouse_button_catcher& r) const
        { return which == r.which && button == r.button; }
};

struct Controller_button_catcher {
        uint16_t which;
        uint16_t button;
        bool operator==(const Controller_button_catcher& r) const
        { return which == r.which && button == r.button; }
};

struct Controller_axis_n_catcher {
        uint16_t which;
        uint16_t axis;
        bool operator==(const Controller_axis_n_catcher& r) const
        { return which == r.which && axis == r.axis; }
};

struct Controller_axis_p_catcher {
        uint16_t which;
        uint16_t axis;
        bool operator==(const Controller_axis_p_catcher& r) const
        { return which == r.which && axis == r.axis; }
};

struct Joystick_button_catcher {
        uint16_t which;
        uint16_t button;
        bool operator==(const Joystick_button_catcher& r) const
        { return which == r.which && button == r.button; }
};

struct Joystick_axis_n_catcher {
        uint16_t which;
        uint16_t axis;
        bool operator==(const Joystick_axis_n_catcher& r) const
        { return which == r.which && axis == r.axis; }
};

struct Joystick_axis_p_catcher {
        uint16_t which;
        uint16_t axis;
        bool operator==(const Joystick_axis_p_catcher& r) const
        { return which == r.which && axis == r.axis; }
};

struct Joystick_hat_catcher {
        uint16_t which;
        uint16_t hat;
        uint16_t value;
        bool operator==(const Joystick_hat_catcher& r) const
        { return which == r.which && hat == r.hat && value == r.value; }
};

struct Quit_catcher {
        bool operator==(const Quit_catcher& r) const { return true; }
};

// The general event catcher is a variant of all possible catcher types.

using Event_catcher = std::variant<
        Keycode_catcher,
        Scancode_catcher,
        Mouse_button_catcher,
        Controller_button_catcher,
        Controller_axis_n_catcher,
        Controller_axis_p_catcher,
        Joystick_button_catcher,
        Joystick_axis_n_catcher,
        Joystick_axis_p_catcher,
        Joystick_hat_catcher,
        Quit_catcher>;

// For each event received, call this function with each catcher that may catch
// the event, and pass the old state of the input.
// Returns the new state for the given input.

struct catch_event_fn {
        bool operator()(const SDL_Event& event, const Event_catcher& catcher, bool old_state) const;

        template<std::input_iterator I, std::sentinel_for<I> S>
        requires std::same_as<std::iter_value_t<I>, Event_catcher>
        bool operator()(const SDL_Event& event, I first, S last, bool old_state) const
        {
                while (first != last)
                        old_state = (*this)(event, *first++, old_state);
                return old_state;
        }

        template<std::ranges::range R>
        requires std::same_as<std::ranges::range_value_t<R>, Event_catcher>
        bool operator()(const SDL_Event& event, R&& r, bool old_state) const
        { return (*this)(event, std::ranges::begin(r), std::ranges::end(r), old_state); }
};

inline constexpr catch_event_fn catch_event;

// Serialize and deserialize event catchers.
// Mostly useful for configuration files.

std::ostream& operator<<(std::ostream& stream, const Event_catcher& catcher);
std::istream& operator>>(std::istream& stream, Event_catcher& catcher);

// Make an event catcher for the given event.

std::optional<Event_catcher> make_event_catcher(const SDL_Event& event);

#endif // SRC_EVENT_H_
