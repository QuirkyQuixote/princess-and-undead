// render.cc - instructions for scene rendering

// Copyright (C) 2021-2023 L. Sanz <luis.sanz@gmail.com>

// This file is part of Princess and Undead.

// Princess and Undead is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Princess and Undead is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Princess and Undead.  If not, see <https://www.gnu.org/licenses/>.


#include "render.h"

Geometry::Geometry()
{
        glBindVertexArray(*vao_);
        glBindBuffer(GL_ARRAY_BUFFER, *vbo_);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) 0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) offsetof(Vertex, u));
}

void Geometry::update(const Vertex* buf, size_t len)
{
        glBindBuffer(GL_ARRAY_BUFFER, *vbo_);
        glBufferData(GL_ARRAY_BUFFER, len * sizeof(Vertex), buf, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        size_ = len;
}

void Geometry::render()
{
        glBindVertexArray(*vao_);
        glDrawArrays(GL_TRIANGLES, 0, size_);
}

void Tile_layer::update(const Nametable& nametable)
{
        std::vector<Vertex> buf;
        auto begin = nametable.begin();
        auto end = nametable.end();
        for (auto it = begin; it != end; ++it)
                push_vertices(buf, {geom::path(begin, it), *it});
        geometry.update(buf.data(), buf.size());
}

void Tile_layer::render()
{
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, *texture);
        geometry.render();
}

void Sprite_layer::update(const std::vector<Sprite>& sprites)
{
        std::vector<Vertex> buf;
        for (auto& s : sprites)
                push_vertices(buf, s);
        geometry.update(buf.data(), buf.size());
}

void Sprite_layer::render()
{
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, *texture);
        geometry.render();
}

void push_vertices(std::vector<Vertex>& buf, Sprite s)
{
        int u = s.c % 32;
        int v = (s.c / 32) % 32;
        GLfloat x0 = (int)s.p.x;
        GLfloat y0 = (int)s.p.y;
        GLfloat x1 = x0 + 8;
        GLfloat y1 = y0 + 8;
        GLfloat u0 = u / 32.0;
        GLfloat v0 = v / 32.0;
        GLfloat u1 = (u + 1) / 32.0;
        GLfloat v1 = (v + 1) / 32.0;
        if ((s.c & Sprite_flags::fliph)) std::swap(u0, u1);
        if ((s.c & Sprite_flags::flipv)) std::swap(v0, v1);
        buf.push_back({x0, y0, u0, v0});
        buf.push_back({x1, y0, u1, v0});
        buf.push_back({x1, y1, u1, v1});
        buf.push_back({x0, y0, u0, v0});
        buf.push_back({x1, y1, u1, v1});
        buf.push_back({x0, y1, u0, v1});
}

