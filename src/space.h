// space.h - declare Geom spaces used by the game

// Copyright (C) 2021-2023 L. Sanz <luis.sanz@gmail.com>

// This file is part of Princess and Undead.

// Princess and Undead is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Princess and Undead is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Princess and Undead.  If not, see <https://www.gnu.org/licenses/>.


#ifndef SRC_SPACE_H_
#define SRC_SPACE_H_

#include "geom/math.h"

// Cells are used address the nametable for rendering and the physics hitmask
// for scenery collision.

using cells8 = geom::Num<int8_t, geom::Unit<1>, std::ratio<1>>;
using cells8v = geom::Vec<cells8, 2>;
using cells8b = geom::Box<cells8, 2>;

using cells16 = geom::Num<int16_t, geom::Unit<1>, std::ratio<1>>;
using cells16v = geom::Vec<cells16, 2>;
using cells16b = geom::Box<cells16, 2>;

using cells32 = geom::Num<int32_t, geom::Unit<1>, std::ratio<1>>;
using cells32v = geom::Vec<cells32, 2>;
using cells32b = geom::Box<cells32, 2>;

constexpr auto operator ""_cl(unsigned long long n) { return cells32(n); }

// Pixels define the scale at which we render (in the textures, not on the
// final screen); each element in the nametable is 8x8 pixels in size.

using pixels8 = geom::Num<int8_t, geom::Unit<1>, std::ratio<1, 8>>;
using pixels8v = geom::Vec<pixels8, 2>;
using pixels8b = geom::Box<pixels8, 2>;

using pixels16 = geom::Num<int16_t, geom::Unit<1>, std::ratio<1, 8>>;
using pixels16v = geom::Vec<pixels16, 2>;
using pixels16b = geom::Box<pixels16, 2>;

using pixels32 = geom::Num<int32_t, geom::Unit<1>, std::ratio<1, 8>>;
using pixels32v = geom::Vec<pixels32, 2>;
using pixels32b = geom::Box<pixels32, 2>;

constexpr auto operator ""_px(unsigned long long n) { return pixels32(n); }

// Points are more refined than pixels so movement is smoother, specially when
// accelerating and decelerating; each cell in the hitmask table is divided in
// 64x64 points.

using points8 = geom::Num<int8_t, geom::Unit<1>, std::ratio<1, 64>>;
using points8v = geom::Vec<points8, 2>;
using points8b = geom::Box<points8, 2>;

using points16 = geom::Num<int16_t, geom::Unit<1>, std::ratio<1, 64>>;
using points16v = geom::Vec<points16, 2>;
using points16b = geom::Box<points16, 2>;

using points32 = geom::Num<int32_t, geom::Unit<1>, std::ratio<1, 64>>;
using points32v = geom::Vec<points32, 2>;
using points32b = geom::Box<points32, 2>;

constexpr auto operator ""_pt(unsigned long long n) { return points32(n); }

// Define vector and box type for boolean values.

using boolv = geom::Vec<bool, 2>;
using boolb = geom::Box<bool, 2>;

#endif // SRC_SPACE_H_
