// option.h - simple option parser

// Copyright (C) 2021-2023 L. Sanz <luis.sanz@gmail.com>

// This file is part of Princess and Undead.

// Princess and Undead is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Princess and Undead is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Princess and Undead.  If not, see <https://www.gnu.org/licenses/>.


#ifndef SRC_OPTION_H_
#define SRC_OPTION_H_

#include <algorithm>
#include <string>

namespace option {

// An option has three fields:
// 
// short_str:      name of the short option
// long_str:       name of the long option
// has_argument:   if true, the option has to be followed by an argument

struct Option {
        char short_str;
        std::string_view long_str;
        bool has_argument;
};

bool operator==(const Option& option, char c)
{ return option.short_str == c; }
bool operator==(const Option& option, const char* s)
{ return option.long_str == s; }

// Sentinel for end of range checks.

struct Option_sentinel {};

// The option iterator is an input iterator that produces the next option from
// the argument list every time operator* is used.
//
// The return value of each operation includes the Option that was matched to
// the current argument and a string view that contains the argument of the
// option, if required.
//
// Option_iterator is also the type of the range of parsed options returned by
// Option_parser; it returns a reference to itself with the begin() function.

template<class OptIter, class ArgIter>
struct Option_iterator {
 public:
        using value_type = std::pair<Option, std::string_view>;
        using pointer = const value_type*;
        using reference = const value_type&;
        using difference_type = ptrdiff_t;
        using iterator_category = std::input_iterator_tag;

 private:
        Option empty{ 0, "", false };
        OptIter first_opt;
        OptIter last_opt;
        ArgIter first_arg;
        ArgIter last_arg;
        const char *sub_arg = nullptr;

        value_type parse_short()
        {
                auto it = std::find(first_opt, last_opt, *sub_arg);
                if (it == last_opt) throw std::runtime_error{
                        "Bad option: -" + std::string(1, *sub_arg)};
                if (*++sub_arg == 0) sub_arg = nullptr;
                if (!it->has_argument) return value_type(*it, "");
                if (first_arg == last_arg) throw std::runtime_error{
                        "Option -" + std::string(1, it->short_str) + " requires value"};
                if (sub_arg != nullptr) throw std::runtime_error{
                        "Option -" + std::string(1, it->short_str) + " requires value"};
                return value_type(*it, *first_arg++);
        }

        value_type parse_long()
        {
                auto it = std::find(first_opt, last_opt, sub_arg);
                if (it == last_opt) throw std::runtime_error{
                        "Bad option: --" + std::string(sub_arg)};
                sub_arg = nullptr;
                if (!it->has_argument) return value_type(*it, "");
                if (first_arg == last_arg) throw std::runtime_error{
                        "Option --" + std::string(it->long_str) + " requires value"};
                return value_type(*it, *first_arg++);
        }

        value_type parse_other()
        {
                auto ret = sub_arg;
                sub_arg = nullptr;
                return value_type(empty, ret);
        }

 public:
        Option_iterator(OptIter first_opt, OptIter last_opt, ArgIter first_arg, ArgIter last_arg)
                : first_opt{first_opt}, last_opt{last_opt}, first_arg{first_arg}, last_arg{last_arg} {}

        bool done() const { return first_arg == last_arg && sub_arg == nullptr; }

        value_type operator*()
        {
                if (sub_arg) return parse_short();
                sub_arg = *first_arg++;
                if (*sub_arg != '-') return parse_other();
                ++sub_arg;
                if (*sub_arg != '-') return parse_short();
                ++sub_arg;
                if (*sub_arg != 0) return parse_long();
                sub_arg = nullptr;
                return value_type(empty, "-");
        }

        void operator++() {}
        void operator++(int) {}

        Option_iterator& begin() { return *this; }
        Option_sentinel end() { return Option_sentinel{}; }
};

template<class OptIter, class ArgIter>
bool operator==(const Option_iterator<OptIter, ArgIter>& i, Option_sentinel s)
{ return i.done(); }

template<class OptIter, class ArgIter>
bool operator==(Option_sentinel s, const Option_iterator<OptIter, ArgIter>& i)
{ return i.done(); }

template<class OptIter, class ArgIter>
bool operator!=(const Option_iterator<OptIter, ArgIter>& i, Option_sentinel s)
{ return !i.done(); }

template<class OptIter, class ArgIter>
bool operator!=(Option_sentinel s, const Option_iterator<OptIter, ArgIter>& i)
{ return !i.done(); }

// The option parser just contains a list of options; all parsing logic is
// handled by Option_iterator.

struct Option_parser {
 private:
        using Base = std::vector<Option>;

        Base options;

 public:
        Option_parser(std::initializer_list<Option> ilist) : options(ilist) {}

        template<class ArgIter>
        Option_iterator<Base::iterator, ArgIter> operator()(ArgIter first, ArgIter last)
        { return Option_iterator<Base::iterator, ArgIter>(options.begin(), options.end(), first, last); }
};

}; // namespace option

#endif // SRC_OPTION_H_
