// mobs.h - types to describe movable objects

// Copyright (C) 2021-2023 L. Sanz <luis.sanz@gmail.com>

// This file is part of Princess and Undead.

// Princess and Undead is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Princess and Undead is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Princess and Undead.  If not, see <https://www.gnu.org/licenses/>.


#ifndef SRC_MOBS_H_
#define SRC_MOBS_H_

#include "physics.h"

// Forward declaration
struct Context;
struct Mob_place;

// Mob types
enum class Mob_type : uint8_t {
        none, rotten, bones, bone, peeper, crawler, crack, mirror, thorns,
        explosion, splatter, coin, dust,
};

// Any mob
struct Mob {
        virtual ~Mob() {}
        virtual void update(Context&) = 0;
        virtual bool dead() const = 0;
};

Mob* make_mob(Mob_place& place);

// Generates a dust cloud
void dust(Context& context, points32v p);

#endif // SRC_MOBS_H_
