// config.cc - load and save configuration

// Copyright (C) 2021-2023 L. Sanz <luis.sanz@gmail.com>

// This file is part of Princess and Undead.

// Princess and Undead is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Princess and Undead is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Princess and Undead.  If not, see <https://www.gnu.org/licenses/>.


#include "config.h"

#include <fstream>

#include "mdea/mdeaxx.h"

#include "media.h"

SDL_AudioFormat get_audio_format_from_name(const std::string_view& name)
{
        if (name == "s8") return AUDIO_S8;
        if (name == "u8") return AUDIO_U8;
        if (name == "s16lsb") return AUDIO_S16LSB;
        if (name == "s16msb") return AUDIO_S16MSB;
        if (name == "s16sys") return AUDIO_S16SYS;
        if (name == "s16") return AUDIO_S16;
        if (name == "u16lsb") return AUDIO_U16LSB;
        if (name == "u16msb") return AUDIO_U16MSB;
        if (name == "u16sys") return AUDIO_U16SYS;
        if (name == "u16") return AUDIO_U16;
        if (name == "u32lsb") return AUDIO_S32LSB;
        if (name == "s32msb") return AUDIO_S32MSB;
        if (name == "s32sys") return AUDIO_S32SYS;
        if (name == "s32") return AUDIO_S32;
        if (name == "f32lsb") return AUDIO_F32LSB;
        if (name == "f32msb") return AUDIO_F32MSB;
        if (name == "f32sys") return AUDIO_F32SYS;
        if (name == "f32") return AUDIO_F32;
        throw std::runtime_error{"Bad audio format: " + std::string(name)};
}

std::string_view get_audio_format_name(SDL_AudioFormat n)
{
        if (n == AUDIO_S8) return "s8";
        if (n == AUDIO_U8) return "u8";
        if (n == AUDIO_S16LSB) return "s16lsb";
        if (n == AUDIO_S16MSB) return "s16msb";
        if (n == AUDIO_S16SYS) return "s16sys";
        if (n == AUDIO_S16) return "s16";
        if (n == AUDIO_U16LSB) return "u16lsb";
        if (n == AUDIO_U16MSB) return "u16msb";
        if (n == AUDIO_U16SYS) return "u16sys";
        if (n == AUDIO_U16) return "u16";
        if (n == AUDIO_S32LSB) return "u32lsb";
        if (n == AUDIO_S32MSB) return "s32msb";
        if (n == AUDIO_S32SYS) return "s32sys";
        if (n == AUDIO_S32) return "s32";
        if (n == AUDIO_F32LSB) return "f32lsb";
        if (n == AUDIO_F32MSB) return "f32msb";
        if (n == AUDIO_F32SYS) return "f32sys";
        if (n == AUDIO_F32) return "f32";
        throw std::runtime_error{"Bad audio format: " + std::to_string(n)};
}

struct Config_loader {
        Config config;

        Config operator()(const std::filesystem::path& path)
        {
                if (auto doc = mdea::document{path}) {
                        parse(doc.root().get_object());
                        return config;
                }
                throw std::runtime_error{path.string() + ": bad JSON"};
        }

        void parse(const mdea::object& obj)
        {
                if (auto x = obj["window"])
                        parse_window(x.get_object());
                if (auto x = obj["render"])
                        parse_render(x.get_object());
                if (auto x = obj["audio"])
                        parse_audio(x.get_object());
                if (auto x = obj["input"])
                        parse_input(x.get_object());
        }

        void parse_window(const mdea::object& obj)
        {
                if (auto x = obj["width"])
                        config.window.width = x.get_long();
                if (auto x = obj["height"])
                        config.window.height = x.get_long();
                if (auto x = obj["fullscreen"])
                        config.window.fullscreen = x.get_long();
                if (auto x = obj["vsync"])
                        config.window.vsync = x.get_long();
        }

        void parse_render(const mdea::object& obj)
        {
                if (auto x = obj["vertex_shader"])
                        config.render.vertex_shader = x.get_string();
                if (auto x = obj["fragment_shader"])
                        config.render.fragment_shader = x.get_string();
        }

        void parse_audio(const mdea::object& obj)
        {
                if (auto x = obj["frequency"])
                        config.audio.frequency = x.get_long();
                if (auto x = obj["format"])
                        config.audio.format = parse_audio_format(x.get_string());
                if (auto x = obj["channels"])
                        config.audio.channels = x.get_long();
                if (auto x = obj["chunksize"])
                        config.audio.chunksize = x.get_long();
                if (auto x = obj["music_volume"])
                        config.audio.music_volume = x.get_long();
                if (auto x = obj["sfx_volume"])
                        config.audio.sfx_volume = x.get_long();
        }

        void parse_input(const mdea::object& obj)
        {
                if (auto x = obj["left"])
                        config.input.left = parse_event_catchers(x.get_array());
                if (auto x = obj["right"])
                        config.input.right = parse_event_catchers(x.get_array());
                if (auto x = obj["up"])
                        config.input.up = parse_event_catchers(x.get_array());
                if (auto x = obj["down"])
                        config.input.down = parse_event_catchers(x.get_array());
                if (auto x = obj["jump"])
                        config.input.jump = parse_event_catchers(x.get_array());
                if (auto x = obj["attack"])
                        config.input.attack = parse_event_catchers(x.get_array());
        }

        std::vector<Event_catcher> parse_event_catchers(const mdea::array& array)
        {
                std::vector<Event_catcher> catchers;
                for (auto x : array)
                        catchers.push_back(parse_event_catcher(x.get_string()));
                return catchers;
        }

        Event_catcher parse_event_catcher(const std::string_view& str)
        {
                Event_catcher catcher;
                std::string buf{str};
                std::istringstream{buf} >> catcher;
                return catcher;
        }

        SDL_AudioFormat parse_audio_format(const std::string_view& str)
        { return get_audio_format_from_name(str); }
};

Config load_config(const std::filesystem::path& path)
{
        Config_loader load;
        return load(path);
}

std::ostream& operator<<(std::ostream& stream, const std::vector<Event_catcher>& catchers)
{
        stream << "[";
        bool first = true;
        for (auto& catcher : catchers) {
                if (first) first = false;
                else stream << ",";
                stream << "\"" << catcher << "\"";
        }
        return stream << "]";
}

std::ostream& operator<<(std::ostream& stream, const Config& config)
{
        stream << "{\n";
        stream << "        \"window\":{\n";
        stream << "                \"width\":" << config.window.width << ",\n";
        stream << "                \"height\":" << config.window.height << ",\n";
        stream << "                \"fullscreen\":" << config.window.fullscreen << ",\n";
        stream << "                \"vsync\":" << config.window.vsync << "\n";
        stream << "        },\n";
        stream << "        \"render\":{\n";
        stream << "                \"vertex_shader\":" << config.render.vertex_shader << ",\n";
        stream << "                \"fragment_shader\":" << config.render.fragment_shader << "\n";
        stream << "        },\n";
        stream << "        \"audio\":{\n";
        stream << "                \"frequency\":" << config.audio.frequency << ",\n";
        stream << "                \"format\":\"" << get_audio_format_name(config.audio.format) << "\",\n";
        stream << "                \"channels\":" << config.audio.channels << ",\n";
        stream << "                \"chunksize\":" << config.audio.chunksize << ",\n";
        stream << "                \"music_volume\":" << config.audio.music_volume << ",\n";
        stream << "                \"sfx_volume\":" << config.audio.sfx_volume << "\n";
        stream << "        },\n";
        stream << "        \"input\":{\n";
        stream << "                \"left\":" << config.input.left << ",\n";
        stream << "                \"right\":" << config.input.right << ",\n";
        stream << "                \"up\":" << config.input.up << ",\n";
        stream << "                \"down\":" << config.input.down << ",\n";
        stream << "                \"jump\":" << config.input.jump << ",\n";
        stream << "                \"attack\":" << config.input.attack << "\n";
        stream << "        }\n";
        stream << "}\n";
        return stream;
}

void save_config(const Config& config, const std::filesystem::path& path)
{
        std::ofstream stream{path.string()};
        if (stream) stream << config;
        else throw std::runtime_error{"Can't open " + path.string()};
}
