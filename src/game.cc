// game.cc - main game logic

// Copyright (C) 2021-2023 L. Sanz <luis.sanz@gmail.com>

// This file is part of Princess and Undead.

// Princess and Undead is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Princess and Undead is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Princess and Undead.  If not, see <https://www.gnu.org/licenses/>.


#include "game.h"

#include <algorithm>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "xdgbds.h"

// size of visible area, in tiles
const cells16v viewport{48, 26};

Audio::Audio(const Audio_config& conf)
{
        if (Mix_OpenAudio(conf.frequency, conf.format, conf.channels, conf.chunksize) != 0)
                throw std::runtime_error{Mix_GetError()};

        Mix_Volume(-1, conf.sfx_volume);
        Mix_VolumeMusic(conf.music_volume);

        sounds.push_back(load_sound(xdg::data::find(BASEDIR "/sounds/bounce.wav")));
        sounds.push_back(load_sound(xdg::data::find(BASEDIR "/sounds/hurt.wav")));
        sounds.push_back(load_sound(xdg::data::find(BASEDIR "/sounds/jump.wav")));
        sounds.push_back(load_sound(xdg::data::find(BASEDIR "/sounds/land.wav")));
        sounds.push_back(load_sound(xdg::data::find(BASEDIR "/sounds/step.wav")));
        sounds.push_back(load_sound(xdg::data::find(BASEDIR "/sounds/swing.wav")));
        sounds.push_back(load_sound(xdg::data::find(BASEDIR "/sounds/mob_die.wav")));
        sounds.push_back(load_sound(xdg::data::find(BASEDIR "/sounds/coin.wav")));
        sounds.push_back(load_sound(xdg::data::find(BASEDIR "/sounds/death.wav")));
        sounds.push_back(load_sound(xdg::data::find(BASEDIR "/sounds/ping.wav")));
}

Audio::~Audio()
{
        Mix_CloseAudio();
}

void Audio::update(Context& context)
{
        for (size_t i = 0; i < sounds.size(); ++i)
                if (context.sounds[i])
                        Mix_PlayChannel(-1, sounds[i].get(), 0);
        context.sounds = 0;
}

// General camera updating is very simple:
// 
// if it's outside the current room, update with outside(), then return enter
// if the camera reached the room and outside if not.
//
// if it's inside the current room, update with inside(), then return exit if
// the camera exited the room and inside if not.

Camera::State Camera::update(Context& context)
{
        if (scrolling) {
                outside(context);
                return scrolling ? State::outside : State::enter;
        }
        inside(context);
        return scrolling ? State::exit : State::inside;
}

void Camera::outside(Context& context)
{
        if (geom::abs(offset.x - context.offset.x) > 8_px)
                context.offset.x = (context.offset.x + offset.x) / 2;
        else
                context.offset.x = offset.x;
        if (geom::abs(offset.y - context.offset.y) > 8_px)
                context.offset.y = (context.offset.y + offset.y) / 2;
        else
                context.offset.y = offset.y;
        if (offset == context.offset)
                scrolling = false;
}

void Camera::inside(Context& context)
{
        if (geom::intersects(context.princess.body.box, context.room.box)) {
                context.offset = recalculate_offset(context);
                return;
        }
        for (auto& r : context.rooms) {
                if (geom::intersects(context.princess.body.box, r.box)) {
                        context.room = r;
                        break;
                }
        }
        offset = recalculate_offset(context);
        scrolling = true;
}

pixels16v Camera::recalculate_offset(Context& context)
{
        pixels16v ret = top_left(context.princess.body.box) - (viewport / 2);
        pixels16b box(context.room.box.min(), context.room.box.max() - viewport);
        ret.x = std::clamp(ret.x, box.l, box.r);
        ret.y = std::clamp(ret.y, box.d, box.u);
        return ret;
}

// Manages high-level rendering logic, so basically it loads a pair of shaders
// as specified in the configuration, holds the tile and sprite layers, and
// calculates the MVP matrix.
//
// The update() function is called to rebuild geometry. The sprite geometry
// will always be rebuilt; the tile geometry will only be rebuild if
// context.update_nametable is true.
//
// The render() function renders the geometry that is built at that moment

Renderer::Renderer(const Render_config& config) :
        program{vertex_shader(xdg::data::find(config.vertex_shader)),
                fragment_shader(xdg::data::find(config.fragment_shader))},
        program_mvp(program, "mvp"),
        program_image(program, "image")
{}

void Renderer::update(Context& context)
{
        if (context.update_nametable) {
                tiles.update(context.nametable);
                context.update_nametable = false;
        }
        sprites.update(context.sprites);
}

void Renderer::render(Context& context)
{
        auto [width, height] = get_viewport();
        glViewport(0, 0, width, height * (int)viewport.y / ((int)viewport.y + 1));
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        pixels16b box{context.offset, context.offset + viewport};
        glUseProgram(*program);
        program_mvp = glm::ortho<float>((int)box.l, (int)box.r, (int)box.u, (int)box.d, 0, 1);
        program_image = 0;
        if (context.princess.health > 0)
                tiles.render();
        sprites.render();
}

// Manages events for the game
//
// At creation time, the class opens every single joystick and game controller
// it can find. This is probably not a good idea, but I'm still experimenting
// with the concept a little.
//
// The update function will consume every queued event from SDL and pass them
// to the event catchers defined in the configuration table, then update the
// input struct with the resulting values.

Event_handler::Event_handler(const Input_config& config) : config{config}
{
        gamepads = open_all_gamepads();
}

void Event_handler::update(Input& input)
{
        SDL_Event event;
        while (SDL_PollEvent(&event))
                handle_event(event, input);
}

void Event_handler::handle_event(const SDL_Event& event, Input& input)
{
        static const std::vector<Event_catcher> quit = {
                Scancode_catcher{SDL_SCANCODE_ESCAPE},
                Quit_catcher{}
        };

        input.left = catch_event(event, config.left, input.left);
        input.right = catch_event(event, config.right, input.right);
        input.up = catch_event(event, config.up, input.up);
        input.down = catch_event(event, config.down, input.down);
        input.jump = catch_event(event, config.jump, input.jump);
        input.attack = catch_event(event, config.attack, input.attack);
        input.terminate = catch_event(event, quit, input.terminate);
}

// The update process starts by moving the camera; the rest depends on what the
// camera returned:
//
// When the camera enters a new room, the princess position is stored in the
// room_spawn variable, and the mob list is populated from the mob places that
// are inside the room.  
//
// If the camera remains inside a room, the game runs: the mobs do things, the
// princess does as the input says... unless the game is frozen, of course.
//
// If the camera exits the current room, all mobs are removed and the sprite
// list is populated only by the princess sprites; the list will remain so
// until the next room is entered.
//
// If the camera is outside the current room, do nothing. The sprites for the
// princess are already in the sprite list, so let this cycle pass and wait
// until the next room is entered.

void clear_mobs(Context& context)
{
        context.mobs.clear();
}

void update_mobs(Context& context)
{
        for (auto& mob : context.mobs) mob->update(context);
        auto dead = [](std::unique_ptr<Mob>& mob){ return mob->dead(); };
        auto it = std::remove_if(context.mobs.begin(), context.mobs.end(), dead);
        context.mobs.erase(it, context.mobs.end());
}

void load_mobs(Context& context)
{
        for (auto i = context.room.first_place; i != context.room.last_place; ++i)
                if (auto mob = make_mob(context.places[i]))
                        context.mobs.emplace_back(mob);
}

Update::Update(const Config& config, Context& context) :
        Main_loop{config.window},
        context{context},
        audio{config.audio},
        renderer{config.render},
        event_handler{config.input}
{
        level_spawn = context.princess.body.box;
}

bool Update::update()
{
        event_handler.update(context.input);
        switch (camera.update(context)) {
         case Camera::State::enter: enter_room(); break;
         case Camera::State::inside: inside_room(); break;
         case Camera::State::exit: exit_room(); break;
         case Camera::State::outside: outside_room(); break;
        }
        renderer.render(context);
        audio.update(context);
        ++context.frame;
        return context.input.terminate;
}

void Update::enter_room()
{
        room_spawn = context.princess.body.box;
        load_mobs(context);
}

void Update::inside_room()
{
        if (context.freeze) {
                --context.freeze;
                return;
        }
        if (context.princess.health <= 0) {
                context.princess.body.vel = {0, 0};
                context.princess.health = 127;
                context.princess.body.box = level_spawn;
                return;
        }
        context.sprites.clear();
        update_mobs(context);
        update_princess(context);
        renderer.update(context);
        if (context.princess.body.box.d > context.room.box.u) {
                context.princess.body.box = room_spawn;
                context.princess.body.vel = {0, 0};
        }
}

void Update::exit_room()
{
        context.sprites.clear();
        clear_mobs(context);
        update_princess(context);
        renderer.update(context);
}

void Update::outside_room()
{
}

