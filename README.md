Princess and Undead
===================

This is a little experiment I started to boost my creativity by forcing myself
to technical limitations similar (but not exactly the same) as the original
NES:

- graphics are limited to two layers: a tile layer and a sprite layer,
- both tiles and sprites are 4-color 8x8 pixels bitmasks,
- each layer can use no more than 256 individual bitmasks,
- each layer can use no more than four color palettes,
- one of the sprite colors is transparent,
- input is limited to four directions and four buttons,
- scrolling is limited to one direction.

Some things are do not comply with the NES:

- the color palette is execrable, don't use it;
- the screen size is small and a different shape;
- the code is small, but not that small;
- the level data will be as large as I want it to;
- audio will feel retro, but that's all.

Installation
------------

The code is written in 2020 ISO C++.  It uses the libraries `stdc++`, `GL`,
`GLM`, `GLEW`, `SDL2`, `SDL2_image`, and `SDL2_mixer`.
It has been tested natively in Debian 12, and successfully cross-compiled for
Windows.

```shell
sudo apt install build-essential
sudo apt install libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev
sudo apt install libgl1-mesa-dev libglm-dev libglew-dev
```

By default, the make scripts installs everything in `prefix=$(HOME)/.local`,
with the executable in `bindir=$(prefix)/bin` and the data in
`datadir=$(prefix)/share/princess-and-undead`; this can be changed by providing
alternative values for `prefix`, `bindir`, or `datadir` to make, but note below
about the paths where the executable searches for data.

```shell
make all install
```

Launch with `princess-and-undead`

The executable searches for data in a `princess-and-undead` directory on paths
according to the XDG base directory specification, in order:

- wherever `$XDG_DATA_HOME` says, or if not defined `$HOME/.local/share`;
- wherever `$XDG_DATA_DIRS` says, or if not defined `/usr/local/share:/usr/share`.

Resource Format
---------------

The graphic set for each layer is stored in a GIF file, 256x256 pixels; this
results in 32x32=1024 bitmasks, but each one repeats four times, each one with
a different color palette to match the arbitrary restrictions.

The current scene is a JSON file created in tiled with three layers named
"collision", "objects", and "tiles":

- collision defines the collision mask for physics: every tile that is not
  unset is considered solid; every one that is, is considered empty.

- objects is an object layer that contains rectangles. The only properties that
  matter are their names and the boxes; these declare mobs, rooms, and the
  princess herself.

- tiles is the background layer and is loaded verbatim into the tile layer.

