#version 330 core

in vec2 fragment_tc;

out vec4 color;

uniform sampler2D image;

void main()
{
        color = texture(image, fragment_tc);
        if (color.a < 0.5) discard;
}
