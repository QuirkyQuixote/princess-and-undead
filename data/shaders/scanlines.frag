#version 330 core

in vec2 fragment_tc;

out vec4 color;

uniform sampler2D image;

void main()
{
        color = texture(image, fragment_tc);
        if (color.a < 0.5) discard;
        if (clamp(max(color.x, max(color.y, color.z)), 0.2, 1.0)
                        < abs(1 - fract(fragment_tc.y * 256) * 2))
                color *= 0.5;
        else
                color *= 1.5;
}
