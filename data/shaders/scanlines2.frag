#version 330 core

in vec2 fragment_tc;

out vec4 color;

uniform sampler2D image;

void main()
{
        color = texture(image, fragment_tc);
        if (color.a < 0.5) discard;
        float x = 1 - abs(1 - fract(fragment_tc.x * 256) * 2);
        float y = 1 - abs(1 - fract(fragment_tc.y * 256) * 2);
        color *= (3.5 * y);
        color *= (0.67 + 0.33 * x);
}
