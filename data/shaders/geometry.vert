#version 330 core

layout(location = 0) in vec2 vertex_pos;
layout(location = 1) in vec2 vertex_tc;

out vec2 fragment_tc;

uniform mat4 mvp;

void main()
{
        gl_Position = mvp * vec4(vertex_pos, 0.0f, 1.0f);
        fragment_tc = vertex_tc;
}
