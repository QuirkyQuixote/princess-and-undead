
# Where this file resides

root_dir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

# When version changes, increment this number

VERSION := 1.0.0

# Determine where we are going to install things

prefix := $(HOME)/.local
bindir := $(prefix)/bin
datadir := $(prefix)/share/princess-and-undead

# According to the GNU Make documentation: "Every Makefile should define the
# variable INSTALL, which is the basic command for installing a file into the
# system.  Every Makefile should also define the variables INSTALL_PROGRAM and
# INSTALL_DATA."

INSTALL := install
INSTALL_PROGRAM := $(INSTALL)
INSTALL_DATA := $(INSTALL) -m 644

# These flags may be overriden by the user

CXXFLAGS ?= -g
CPPFLAGS ?= $(shell pkg-config --cflags sdl2 SDL2_image SDL2_mixer glew gl)
LDLIBS ?= $(shell pkg-config --libs sdl2 SDL2_image SDL2_mixer glew gl)

# These flags are necessary

override CPPFLAGS += -DVERSION=\"$(VERSION)\"
override CPPFLAGS += -DBASEDIR=\"princess-and-undead\"
override CPPFLAGS += -I$(root_dir)geom
override CPPFLAGS += -I$(root_dir)mdea
override CPPFLAGS += -I$(root_dir)xdgbds

override CXXFLAGS += -std=c++20
override CXXFLAGS += -fPIC
override CXXFLAGS += -MMD
override CXXFLAGS += -Wall
override CXXFLAGS += -Werror
override CXXFLAGS += -Wfatal-errors

# Compilation takes place in...

vpath %.cc $(root_dir)src

# And now, the rules.

.PHONY: all
all: submodules main

.PHONY: clean
clean:
	$(RM) *.o
	$(RM) *.d
	$(RM) main
	make -f $(root_dir)mdea/Makefile clean
	make -C $(root_dir)geom clean

.PHONY: install
install: all
	$(INSTALL) -d $(DESTDIR)$(bindir)
	$(INSTALL_PROGRAM) main $(DESTDIR)$(bindir)/princess-and-undead
	$(INSTALL) -d $(DESTDIR)$(datadir)
	$(INSTALL) -d $(DESTDIR)$(datadir)/textures
	$(INSTALL_DATA) $(root_dir)data/textures/sprites.gif $(DESTDIR)$(datadir)/textures
	$(INSTALL_DATA) $(root_dir)data/textures/tiles.gif $(DESTDIR)$(datadir)/textures
	$(INSTALL) -d $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) $(root_dir)data/levels/untitled.json $(DESTDIR)$(datadir)/levels
	$(INSTALL) -d $(DESTDIR)$(datadir)/sounds
	$(INSTALL_DATA) $(root_dir)data/sounds/step.wav $(DESTDIR)$(datadir)/sounds
	$(INSTALL_DATA) $(root_dir)data/sounds/land.wav $(DESTDIR)$(datadir)/sounds
	$(INSTALL_DATA) $(root_dir)data/sounds/swing.wav $(DESTDIR)$(datadir)/sounds
	$(INSTALL_DATA) $(root_dir)data/sounds/hurt.wav $(DESTDIR)$(datadir)/sounds
	$(INSTALL_DATA) $(root_dir)data/sounds/jump.wav $(DESTDIR)$(datadir)/sounds
	$(INSTALL_DATA) $(root_dir)data/sounds/bounce.wav $(DESTDIR)$(datadir)/sounds
	$(INSTALL_DATA) $(root_dir)data/sounds/mob_die.wav $(DESTDIR)$(datadir)/sounds
	$(INSTALL_DATA) $(root_dir)data/sounds/coin.wav $(DESTDIR)$(datadir)/sounds
	$(INSTALL_DATA) $(root_dir)data/sounds/death.wav $(DESTDIR)$(datadir)/sounds
	$(INSTALL_DATA) $(root_dir)data/sounds/ping.wav $(DESTDIR)$(datadir)/sounds
	$(INSTALL) -d $(DESTDIR)$(datadir)/shaders
	$(INSTALL_DATA) $(root_dir)data/shaders/geometry.vert $(DESTDIR)$(datadir)/shaders
	$(INSTALL_DATA) $(root_dir)data/shaders/geometry.frag $(DESTDIR)$(datadir)/shaders
	$(INSTALL_DATA) $(root_dir)data/shaders/scanlines.frag $(DESTDIR)$(datadir)/shaders
	$(INSTALL_DATA) $(root_dir)data/shaders/scanlines2.frag $(DESTDIR)$(datadir)/shaders

.PHONY: uninstall
uninstall:
	-$(RM) $(DESTDIR)$(bindir)/princess-and-undead
	-$(RM) -d $(DESTDIR)$(bindir)
	-$(RM) $(DESTDIR)$(datadir)/textures/sprites.gif
	-$(RM) $(DESTDIR)$(datadir)/textures/tiles.gif
	-$(RM) -d $(DESTDIR)$(datadir)/textures
	-$(RM) $(DESTDIR)$(datadir)/levels/untitled.json
	-$(RM) -d $(DESTDIR)$(datadir)/levels
	-$(RM) $(DESTDIR)$(datadir)/sounds/step.wav
	-$(RM) $(DESTDIR)$(datadir)/sounds/land.wav
	-$(RM) $(DESTDIR)$(datadir)/sounds/swing.wav
	-$(RM) $(DESTDIR)$(datadir)/sounds/hurt.wav
	-$(RM) $(DESTDIR)$(datadir)/sounds/jump.wav
	-$(RM) $(DESTDIR)$(datadir)/sounds/bounce.wav
	-$(RM) $(DESTDIR)$(datadir)/sounds/mob_die.wav
	-$(RM) $(DESTDIR)$(datadir)/sounds/coin.wav
	-$(RM) $(DESTDIR)$(datadir)/sounds/death.wav
	-$(RM) $(DESTDIR)$(datadir)/sounds/ping.wav
	-$(RM) -d $(DESTDIR)$(datadir)/sounds
	-$(RM) $(DESTDIR)$(datadir)/shaders/geometry.vert
	-$(RM) $(DESTDIR)$(datadir)/shaders/geometry.frag
	-$(RM) $(DESTDIR)$(datadir)/shaders/scanlines.frag
	-$(RM) $(DESTDIR)$(datadir)/shaders/scanlines2.frag
	-$(RM) -d $(DESTDIR)$(datadir)/shaders
	-$(RM) -d $(DESTDIR)$(datadir)

.PHONY: submodules
submodules:
	make -f $(root_dir)mdea/Makefile CC="$(CC)" AR="$(AR)"
	make -C $(root_dir)geom

main: main.o princess.o mobs.o game.o context.o config.o event.o render.o media.o physics.o libmdea.a
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $@ $^ $(LOADLIBES) $(LDLIBS)

-include $(shell find -name "*.d")

